﻿

<!--#include file="establish.asp"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Display</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script src="scripts/jquery-1.4.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
<script>
console.log('start');
<%
set lstStatic=Server.CreateObject("Scripting.Dictionary")

sql="select * FROM slides where (is_live=1 and design_style between 100006 and 100009) AND " &_ 	
		"( (days_active like '%" & weekday(date)-1 & "%') " &_
		"OR  ( start_on<='" & date & "' and (end_on>='" & date & "' or end_on IS NULL)  )  )" &_
		"AND ( (start_time<='" & time & "' AND (end_time>='" & time & "' OR end_time IS NULL)) OR start_time IS NULL ) " &_
		" ORDER BY design_style "
%>

<%
set rx=conn.execute(sql)
do until rx.EOF
	lstStatic(rx("design_style") & "") = rx("content_top")
	%>
    
    <%
	rx.movenext
loop
set rx=nothing
%>
</script>
<!--#include virtual="overrides.asp"-->
     
    <% minyan_info = getMinyanInfo	%>
 
     
     
   
        
     
     
     <div id="s_live_traffic" class="mid_slide">
     </div>
     
	
    <div id="all_slides">
    </div>



        
        
        
<div id="left_content_upper">
	
    <%=lstStatic("100006")%>
    


    </div>
    
</div>

<div id="left_content_lower"><%=lstStatic("100007")%></div>	


<div id="bs_logo">

<img src="images/ext_logo.png" />
</div>


<div id="right_content_upper"><%'=lstStatic("100008")%>

	<div>
        Zemanim Times For Brooklyn, NY<br /><br />
        <div style=" line-height:30px; margin-left:10px" id="zmantimes">
            
        
        <br />
        </div>

        <div align="center"><br />
            Time Now:<br />
            <div id="timenow"></div><br />
            <div id="weather" style="font-size:20px; width:335px"></div>									                                                                    
        </div>

</div>	

<div id="right_content_lower"><%=lstStatic("100009")%></div>	



<div id="right_content" style="display:none;">
    <div id="prevminyan" style="font-size:32px; color:#000000; text-align:center; font-weight:bold; margin-top:20px; background-color:#f57414;">
    Previous Minyan: 
    </div>
    <div id="last_minyan" style=" min-height:110px; padding:20px; font-size:40px; color:#000000; text-align:center; font-weight:bold; background-color:#f57414;">
    </div>

<div style="font-size:42px; color:#FFFFFF; text-align:center; font-weight:bold; margin-top:20px;  background-color:#b92a25;">
Next Minyan: 
</div>
<div id="current_minyan" style=" min-height:140px; padding:20px; font-size:50px; color:#FFFFFF; text-align:center; font-weight:bold;  background-color:#b92a25;">
</div>

<div style="color:#FFFFFF; margin-top:20px; padding:10px; background-color:#0d3860;">
<B style="font-size:16px; color:#FFFFFF; font-weight:bold;">Upcoming Events:</B><br /><br />


<div id="recent_news" style="color:#FFFFFF; display:none;">
<%
'sql="SELECT * FROM dynamic_content WHERE rec_id='100000'"
'set rxz = conn.execute(sql)
'IF NOT rxz.eof then response.Write(rxz("content"))
'rxz.close
'set rxz=nothing

%>
</div> 
</div>


</div>


<div id="bottom_right" style="position:absolute; top:1010px; left:1600px; color:#CCCCCC; font-size:16px; text-align:center; font-family: Verdana, Arial;">

</div>


<script language="javascript">

trecs = "";
tsecs = "";
currslide = 0;
stopslides = 0;
delayslides = 0;
animstyles = "";

$(document).ready(function(){

		updateClock();
		updataAjax();
		updataDaily();			
		refreshPage();	
			
		} 
		
		);	


function loadSlideVals() {
	delayslides = 12000
	trecs = $("#sliderecs").html();
	trecs = trecs.split(",");
	tsecs = $("#slidetimes").html();
	tsecs = tsecs.split(",");
    animstyles = $("#animstyles").html();
    animstyles = animstyles.split(",");
	currslide = 0;
	stopslides = 0;
	slideIn();
}

function slideIn() {
	if(stopslides != 0) { return; }
	if(currslide>trecs.length-1) { currslide = 0 };

    //$("s_"+trecs[currslide]).fadeIn("display","inline");
	if(animstyles[currslide] == "100002") {
      $("#s_"+trecs[currslide]).animate({ left: 390}, 1000, function() {
		    delayOut(2000)
		    });	
    }

    else {
        $("#s_"+trecs[currslide]).animate({ left: 390}, 1000, function() {
		    delayOut(2000)
		    });	
    }



}


function delayOut(numsecs) {
	if(stopslides == 0) { setTimeout("slideOut()",tsecs[currslide]); }
	
}

function slideOut() {
	$("#s_"+trecs[currslide]).animate({ left: -1200}, 1000, function() {
		$("#s_"+trecs[currslide]).css("left",2000);
		currslide++;		
		if(stopslides == 0) { slideIn() }
		});	

}




var dorefresh = 0;

function updateClock ( )
{
  var currentTime = new Date ( );

  var currentHours = currentTime.getHours ( );
  var milHours = currentTime.getHours ( );
  var currentMinutes = currentTime.getMinutes ( );
  var currentSeconds = currentTime.getSeconds ( );
  var thisDay=currentTime.getDay()
  var milMins = currentTime.getMinutes ( );

  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = ( currentHours < 12 ) ? "<span style='font-size:32px;'>AM</span>" : "<span style='font-size:32px;'>PM</span>";

  // Convert the hours component to 12-hour format if needed
  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + " " + timeOfDay;

  // Update the time display
  document.getElementById("timenow").innerHTML = currentTimeString;
  minyanUpdate(milHours, milMins, thisDay);
  
  dorefresh++;
  if(milHours == 0 && milMins == 10) {
  if(dorefresh>3) {
  	window.location.href="<%=thiswebsite%>displaycontent.asp?doref=<%=timer%>";
  }
  }
  
  if(milMins == 18) {
  //	getURL("http://www.dealshul.com/display/yweather.asp","");
  }
  
  setTimeout("updateClock()",30000);	
		
}


function refreshPage() {
	 $.ajax({ url:'dorefresh.asp', success: function(data) { 
			if(data == "YES") {
				window.location.href="<%=thiswebsite%>displaycontent.asp?doref=<%=timer%>"	
				}
			} 
		
		});
		
	 $.ajax({ url:'<%=thiswebsite%>/hebcal.asp?doref=<%=timer%>', success: function(data) { 
			if(data.length>50) {
				$("#recent_news").html(data);
				}
			} 
		
		});		
		
		setTimeout("refreshPage()",10000);
	
}


function updataAjax() {


		
  //$.ajax({ url:'yahoonews.asp', success: function(data) { 
//		$("#recent_news").html(data) 
//		} 
		
//		});		
		
  $.ajax({ url:'traffic.asp', success: function(data) { 
		$("#s_live_traffic").html(data) 
		} 
		
		});
		
		setTimeout("updataAjax()",300000);
}





function updataDaily() {
	$("#all_slides").html("");
	stopslides = 1;
		
		minutesx = 10
		mindelay = minutesx * 60 * 1000
		//mindelay = 30000

  $.ajax({ url:'zmanim.asp', success: function(data) { 

		$("#zmantimes").html(data) 
		} 
		
		});
		
		
  $.ajax({ url:'yweather.asp', success: function(data) { 
  		tdata = data.replace("Forecast:","Forecast:");
		tdata = tdata.replace("Wed - ","<br><b>Wednesday</b><br>");
		tdata = tdata.replace("Sun - ","<br><b>Sunday</b><br>");
		tdata = tdata.replace("Mon - ","<br><b>Monday</b><br>");
		tdata = tdata.replace("Tue - ","<br><b>Tuesday</b><br>");
		tdata = tdata.replace("Thu - ","<br><b>Thursday</b><br>");
		tdata = tdata.replace("Fri - ","<br><b>Friday</b><br>");
		tdata = tdata.replace("Sat - ","<br><b>Saturday</b><br>");
		$("#weather").html(tdata) 
		} 
		
		});		
		


	$.ajax({ url:'slides.asp', success: function(data) { 		
			$("#all_slides").html(data) 
			setTimeout("loadSlideVals()",delayslides);		
		}
		
		});	
		
		setTimeout("updataDaily()",mindelay);	
			
}

function minyanUpdate(milHours, milMins, thisDay ) {

  var lastMinyan = "";
  var currentMinyan = "";

<% 
	breakt = split(minyan_info,VbCrLf)
	for x=0 to ubound(breakt) 
		ampm=0
		if len(breakt(x))<6 then exit for
		'response.Write breakt(x) 
		bt=split(breakt(x),"|") 	
		if instr(lcase(bt(0)),"pm")>0 then ampm=12
		minyan_time=replace(replace(bt(0),"pm",""),"am","")
		btx = split(minyan_time,":")
		minyan_hour=btx(0)+ampm:minyan_minute=trim(btx(1))
		%>
		
		if(<%=minyan_hour%> == milHours) {
			if(<%=int(minyan_minute)%>>=milMins) {
				currentMinyan = '<%=replace(replace(breakt(x),"|"," - "),"'","\'")%>'
				updateMinyanText(currentMinyan,lastMinyan);
				return;
			}
		}
		else if(<%=minyan_hour%>>milHours) {
				currentMinyan = '<%=replace(replace(breakt(x),"|"," - "),"'","\'")%>'
				updateMinyanText(currentMinyan,lastMinyan);
				return;
		}	
		lastMinyan = '<%=replace(replace(breakt(x),"|"," - "),"'","\'")%>';	
<%		
	next
%>
updateMinyanText(currentMinyan,lastMinyan);
	
	// alert(<%=int(minyan_minute)%>+"="+milMins)
}

function updateMinyanText(data1,data2) {
	$("#current_minyan").html(data1);
	$("#last_minyan").html(data2);
}

</script>



</div>
</body>
</html>

















































