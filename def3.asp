﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Display</title>
<script type="text/javascript" src="scripts/jsAnim.js"></script> 
<style>
body {margin:0; padding:0; overflow:hidden; }
</style>



</head>
<body >

<div style="width:1366px; height:768px; background:url(bgx.jpg); overflow:hidden;">
<div style="float:left; width:1070; height:720px; margin-top:10px;" >
	<div style="width:1070px; height:1px;"></div>
	<div id="sched1"  style="position:relative; display:none; left:1100px;"><img src="sched2.png" height="700" /></div>
    <div id="sched2" style="position:relative; display:none; left:1100px;"><img src="sched1a.png" height="700" /></div>
    <div id="sched3" style="position:relative; display:none; left:1100px;"><img src="sched3.png" height="700" /></div>
     <div id="sched4" style="position:relative; display:none; left:1px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:36px; color:#ffffff">
  <table width="800" align="center" style="margin-top:100px;" cellpadding="10" cellspacing="10"  bgcolor="#566f8f" border="5" bordercolor="#162d4f">
  <tr><td align="center">
    <strong>Minha / Arbit Minyanim</strong><br /></td></tr>
	<tr><td>6:45	-	Main Synagogue</td></tr>
	<tr><td bgcolor="#333333">7:00</strong>	-	Back Synagogue</td></tr>
    <tr><td>7:15</strong>	-	Midrash (Middle Room)</td></tr>
   <tr><td bgcolor="#333333">7:30</strong>	-	Back Synagogue</td></tr></table>
   

     </div>

     <div id="schedkotel" style="position:relative;  margin-top:10px;  left:1500px;   font-family:Verdana, Arial, Helvetica, sans-serif; font-size:42px; color:#ffffff; display:none">
	
  <table width="800" align="center" style="margin-top:50px;" cellpadding="10" cellspacing="10"  bgcolor="#566f8f" border="5" bordercolor="#162d4f">
  <tr><td align="center">
    <strong>Live Kotel Cam</strong><br /><span style="font-size:24px;">(Off on Shabbat)</span></td></tr>
	<tr><td align="center">
    
	<iframe src="http://www.dealshul.com/display/kotel.asp?w=600&h=400" frameborder="0" width="600" height="400" marginheight="0" marginwidth="0"></iframe>
    
    </td></tr></table>
     </div>   
     
     <div id="sched6" style="position:relative; display:none;  left:1500px;">

      <table width="900" align="center"   cellpadding="20" cellspacing="25"  bgcolor="#566f8f" border="0" style="border:solid 5px #162d4f;margin-top:70px;  font-family:Verdana, Arial, Helvetica, sans-serif; font-size:42px; color:#ffffff">
      
      <tr><td align="center"  style="border:solid 1px #162d4f;">
  <strong>** NEW MINYANIM **</strong><br /></td></tr>
  <tr><td>
  <table width="100%" cellpadding="5" cellspacing="0">
   <tr align="center"><td>5:15 AM</td><td>Shaharit</td><td>Midrash</td></tr>
    <tr align="center" style="font-size:36px;"><td>(5:05 AM</td><td>Mon/Th)</td><td></td></tr>
    <tr align="center" bgcolor="#333333"><td>7:10 AM</td><td>Shaharit</td><td>Midrash</td></tr>
    <tr align="center"><td>6:30 PM</td><td>Minha</td><td>Main Synagogue</td></tr>
	 <tr align="center" bgcolor="#333333"><td>8:00 PM</td><td>Arbit</td><td>Main Synagogue</td></tr>
	 <tr align="center"><td>8:40 PM</td><td>Arbit</td><td>Midrash</td></tr>
            
    </table></td>
    </tr></table>      

     
     </div>
         <div id="schedt1" style="position:relative; display:none; left:1500px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:36px; color:#ffffff; ">

     <table width="800" align="center" style="margin-top:10px;" cellpadding="10" cellpadding="10" cellspacing="15"  bgcolor="#566f8f" border="5" bordercolor="#162d4f">
   <tr align="centeR"> <td><strong>FIRST MINYAN BREAKFAST</strong><br />
		Dedicated in Loving Memory Of:</td></tr><tr align="centeR"><td bgcolor="#333333">AVAILABLE <!--Yitzhak Ben Tera Esther ע"ה--> <br />

</td></tr>
        <tr align="centeR"><td>By:
		<strong></strong></td></tr>
         <tr align="centeR"><td style="border:0"></td></tr>    
     <tr  height="42" align="centeR"> <td><strong>SHABBAT KIDDUSH</strong><br />
		Dedicated in Loving Memory Of:</td></tr>
        <tr align="centeR"> <td bgcolor="#333333">AVAILABLE
</td></tr>
        <tr align="centeR"><td>By:
		 </td></tr>
		
    
    </table>  
      
     
     </div>
     
         
     <div id="schedt4" style="position:relative; display:none; left:1100px; margin-top:50px;  font-family:Verdana, Arial, Helvetica, sans-serif; font-size:36px; color:#ffffff;">
    <table width="800" align="center" style="margin-top:0px;" cellpadding="10" cellspacing="40"  bgcolor="#566f8f" border="5" bordercolor="#162d4f" style="border:solid 5px #162d4f;">
   <tr> <td align="center">
    <strong>SHABBAT KI TABO</strong><br />
		<hr />
 <strong> SEUDAH SHELISHIT</strong><br />
		Dedicated in Memory Of:</td></tr>
        <tr> <td bgcolor="#333333" align="center">Joseph Tobias  ע"ה <br />
Yoseph Ben Esther ע"ה

</td></tr>
        <tr><td align="center">
		<strong>By:<BR /> The Tobias Family</strong></td></tr>
        
		
    
    </table>  
    </td></tr></table>      
     
     </div>     
     
     <div id="schedt5" style="position:relative; display:none; left:1100px; margin-top:0px;  font-family:Verdana, Arial, Helvetica, sans-serif; font-size:46px; color:#ffffff;">
     <table width="800" align="center" style="margin-top:80px;" cellpadding="10" cellspacing="40"  bgcolor="#566f8f" border="5" bordercolor="#162d4f" >
   <tr> <td align="center">
    <strong>SHABBAT PERASHAT EKEB</strong><br />
		 <br />
        </td></tr>
        <tr> <td bgcolor="#333333" align="center"><strong>HATARAT NEDARIM</strong></td></tr>
        <tr><td align="center">Saturday Night<br />
		Approx: <strong>9:10 pm</strong>
		</td></tr>
        
		
    
    </table>  
 
     
     </div>         
     <div id="schedt6" style="position:relative; display:none; left:1100px; margin-top:0px; color:#ffffff;">
     <table width="800" align="center" style="margin-top:30px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:46px; " cellpadding="10" cellspacing="40"  bgcolor="#566f8f" border="5" bordercolor="#162d4f" >
   <tr> <td align="center">
    <strong>Holidays 2010</strong><br />
        </td></tr>
        <tr> <td bgcolor="#333333" align="center"><strong>HOLIDAY SEATS AVAILABLE</strong></td></tr>
        <tr><td align="center">Contact<br />
	<strong>Mersh Franco</strong> <br />or another committee member
		</td></tr>
    
    </table>      
    
    </div>
    
    <div id="schedt7" style="position:relative; display:none; left:1100px; margin-top:0px; color:#ffffff;">
         <table width="800" align="center" style="margin-top:60px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:42px; " cellpadding="10" cellspacing="40"  bgcolor="#566f8f" border="5" bordercolor="#162d4f" >
   <tr> <td align="center">
    <strong>Sunday August 15th</strong><br />
        </td></tr>
        <tr> <td bgcolor="#333333" align="center"><strong>** TEFILLIN CHECKING **</strong></td></tr>
        <tr><td align="center"> Soferim will be at the shul this Sunday from 6:45 am on hand and ready to check and adust tefillin.
		</td></tr>
    
    </table> 
    </div>
    
    <div id="schedt8" style="position:relative; display:none; left:1100px; margin-top:0px; color:#ffffff;">
     <table width="800" align="center" style="margin-top:60px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:42px; " cellpadding="10" cellspacing="40"  bgcolor="#566f8f" border="5" bordercolor="#162d4f" >
   <tr> <td align="center">
    <strong>Digital Information Sponsored:</strong><br />
        </td></tr>
        <tr> <td bgcolor="#333333" align="center">For Refuah Shelemah of <br /><strong>Ezra Refael Ben Mazal</strong></td></tr>
        <tr><td align="center"> Contact a committee member or visit <strong>www.dealshul.com</strong> to sponsor this digital display.
		</td></tr>
    
    </table>      
    
    </div>    
    
    <div id="schedt9" style="position:relative; display:none; left:1100px; margin-top:0px; color:#ffffff;">
    
          <table width="900" align="center"   cellpadding="10" cellspacing="25"  bgcolor="#566f8f" border="0" style="border:solid 5px #162d4f;margin-top:0px;  font-family:Verdana, Arial, Helvetica, sans-serif; font-size:42px; color:#ffffff">
      
      <tr><td align="center"  style="border:solid 1px #162d4f;">
  <strong style="font-size:72px;">והיו עיניך רואות את מורך</strong><br /></td></tr>
 	<tr><td align="center" style="font-size:12px;"><img src="gallery/poratyosefx.jpg" width="550" /><br /><br />
    <div style="width:600px; text-align:left; line-height:16px;">
<strong>TOP ROW (LEFT TO RIGHT)</strong> Rabbi BenSion Abu-Shaul , Rabbi Rabbi Sion Levy, Rabbi Yosef Ades, Rabbi Eliyahu Aboud<br />
<strong>MIDDLE ROW: </strong> Rabbi Yosef Elnadav, Rabbi Eaphael Ades, Rabbi Abraham Shrem, Rabbi Ezra Ades, Rabbi Yoseph Raful, Rabbi David Sheloush<br />

<strong>FRONT ROW:</strong>Rabbi Chaim Levy, Rabbi Pinhas Vaknin, Rabbi Shabetai Atoon,  Rabbi Ovadiah Yosef, Rabbi Baruch Ben Haim, Rabbi Sadya Lofes, Rabbi Ezra Shayo
	</div>
</td></tr>
    </table>
    </div>
    
<script>

allfs="schedt4,schedt1,schedt9,schedt8,sched3";
allspeeds="8000,8000,8000,4000,3000"

//allfs="schedt4,schedt1,schedt2,schedt3";
//allspeeds="3000,8000,8000,8000"

<%
IF weekday(date)<>7 then %>
allfs = allfs + ",schedkotel";
allspeeds = allspeeds + ",12000";
<%
end if 
%>

slides=allfs.split(",");
speeds=allspeeds.split(",");
currentslide = 0;
currentspeed = 1000;
currentdiv = "";
	var managerx = new jsAnimManager(20); 
	
function moveSchedx() {
lastdiv = currentdiv;
currentdiv = slides[currentslide];
currentspeed = speeds[currentslide];
currentslide++;
if(currentslide == slides.length) {
	currentslide = 0;
	}
//alert(currentdiv);
moveSchedIn();
}
function moveSchedIn() {
	document.getElementById(currentdiv).style.display = "";
	var animx = managerx.createAnimObject(currentdiv);  
	animx.add({property:  Prop.left, to: 10, ease: jsAnimEase.backout(0.5), duration: 1000, onComplete: moveSchedOut });
	
}

function moveSchedOut() {  
	var animx = managerx.createAnimObject(currentdiv);  
	animx.add({property: Prop.wait, duration: currentspeed});
	animx.add({property:  Prop.left, to: -1201, ease: jsAnimEase.backout(0.5), duration: 1000, onComplete: hideIt });
}

function hideIt() {
	document.getElementById(currentdiv).style.display = "none";
	document.getElementById(currentdiv).style.left = "1200px";
	moveSchedx();
}

moveSchedx();
</script>
<div style="position:absolute; top:0px; left:1080px; width:290; font-family:Arial, Helvetica, sans-serif; color:#efefef; font-size:18px; margin-top:20px; ">
<SCRIPT TYPE="text/javascript" LANGUAGE="javascript" SRC="http://www.paskie.com/zman/default.asp?webserve=1&city=10310">
    </SCRIPT>
    ZEMANIM TIMES FOR DEAL, NJ<br /><br />
    <table>
                                        <script language="javascript">
                                        
                                        function showZmanx() {
                                        
                                            bx = zmansx.split("<BR>");
                                            for(i=0;i<bx.length-1;i++) {
                                                bx2=bx[i].split(",");
                                                document.write("<tr><td>"+bx2[0]+"</td><td style='padding-left:30px;'>"+bx2[1]+"</td></tr>");
                                            }
                                        }
                                        
                                        showZmanx();
                                        </script>
                                        </table>
                                        
                                        <br />
                                        <div align="center"><br />
										Time Now:<br />
                                        <div id="timenow" style="font-size:48px;"></div><br />
                                        <div id="weather" style="font-size:14px">aaa</div>
										
                                                                              
</div>

</div>


<div id="nextminyan"  style="font-size:42px; position:absolute; top:712px; left:20px; font-family:Verdana, Arial, Helvetica, sans-serif; color:#2f4f76">

</div>
<div id="nextminyan"  style="font-size:42px; position:absolute; top:718px; left:1130px; font-family:Verdana, Arial, Helvetica, sans-serif; color:#2f4f76">
<img src="images/ext_logo.png" />
</div>


<script language="javascript">
var dorefresh = 0;
function updateClock ( )
{
  var currentTime = new Date ( );

  var currentHours = currentTime.getHours ( );
  var milHours = currentTime.getHours ( );
  var currentMinutes = currentTime.getMinutes ( );
  var currentSeconds = currentTime.getSeconds ( );
  var thisDay=currentTime.getDay()
  var milMins = currentTime.getMinutes ( );

  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

  // Convert the hours component to 12-hour format if needed
  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + " " + timeOfDay;

  // Update the time display
  document.getElementById("timenow").innerHTML = currentTimeString;
  document.getElementById("nextminyan").innerHTML = nextMinyan(milHours, milMins, thisDay);
  dorefresh++;
  if(milHours == 0 && milMins == 10) {
  if(dorefresh>3) {
  	window.location.href="http://www.dealshul.com/display/def3.asp?doref=<%=timer%>";
  }
  }
  
  if(milMins == 18) {
  	getURL("http://www.dealshul.com/display/yweather.asp","");
  }
  
  setTimeout("updateClock()",30000);
  
}
updateClock();

function nextMinyan(h,m,d) {
	//txx = h+' '+m+' '+d;
	tempmsg = "Next Minyan Will Appear Here When Applicable";
	txx = tempmsg;
	
	if(h < 13) {
		if(d == 2 || d == 3 || d == 5) {
			//tues, wed, fri,
			if(h<6 && m<31) {
				txx = "Vatikin - Main / 5:15 - Midrash"	}
			else if(h == 6 && m<6) {
				txx = "6:00 Minyan - Midrash"		}
			else if(h == 6 && m<30) {
				txx = "6:20 Minyan - Kollel"		}
			else if((h == 6 && m>29) || (h == 7 && m<11)) {
				txx = "7:00 Minyan - Main Synagogue"		}
			else if(h == 7 && m>10 && m<36) {
				txx = "7:30 Minyan - Kollel"		}
			else if(h == 7 && m>35 && m<53) {
				txx = "7:45 Minyan - Main Synagogue"		}
			else if((h == 7 && m>52) || (h == 8 && m<10)) {
				txx = "8:00 Minyan - Midrash"		}
			else if(h == 8 && m>59)  {
				txx = "8:45 Minyan - Main Synagogue"		}
		}
		else if(d ==0) {
			//tues, wed, thurs,
			if(h<6 && m<31) {
				txx = "Vatikin - Main Synagogue"	}
			else if(h == 6 && m<15) {
				txx = "6:00 Minyan - Midrash"		}
			else if((h == 6 && m>14) || (h == 7 && m<11)) {
				txx = "7:00 Minyan - Main Synagogue"		}
			else if(h == 7 && m>10 && m<36) {
				txx = "7:30 Minyan - Kollel"		}
			else if(h == 7 && m>35 && m<53) {
				txx = "7:45 Minyan - Main Synagogue"		}
			else if((h == 7 && m>52) || (h == 8 && m<10)) {
				txx = "8:00 Minyan - Midrash"		}
			else if(h == 8 && m>59)  {
				txx = "8:45 Minyan - Main Synagogue"		}
		}	
		if(d == 1 || d == 4) {
			//tues, wed, thurs,
			if(h<6 && m<31) {
				txx = "Vatikin - Main / 5:15 - Midrash"	}
			else if(h == 6 && m<6) {
				txx = "6:00 Minyan - Midrash"		}
			else if(h == 6 && m<30) {
				txx = "6:20 Minyan - Kollel"		}
			else if((h == 6 && m>29) || (h == 7 && m<11)) {
				txx = "7:00 Minyan - Main Synagogue"		}
			else if(h == 7 && m>10 && m<36) {
				txx = "7:30 Minyan - Kollel"		}
			else if(h == 7 && m>35 && m<53) {
				txx = "7:45 Minyan - Main Synagogue"		}
			else if((h == 7 && m>52) || (h == 8 && m<10)) {
				txx = "8:00 Minyan - Midrash"		}
			else if(h == 8 && m>9)  {
				txx = "8:45 Minyan - Main Synagogue"		}
		}			
		
		
	}
		
	else if(h > 12) {

		if(d == 5) {
			if(h == 18 && m<50)  {
				txx = "6:30 Minyan - Main Synagogue"	}
			else if((h == 18 && m>49) && (h == 19 && m<20)) {
				txx = "7:00 Minyan - Back Synagogue"		}
			else if(h == 19 && m>19 && m<50) {
				txx = "7:30 Minyan - Midrash"		}
			else if((h == 19 && m>49) || (h == 20 && m<30)) {
				txx = "Last Minyan - Main Synagogue"		}			
		}
		else if(d<5) {
			if(h < 18 || (h == 18 && m<41) )  {
				txx = "6:30 Minyan - Midrash"	}
			else if((h == 18 && m>40) || (h == 18 && m<51) )  {
				txx = "6:45 Minyan - Main Synagogue"	}
			else if((h == 18 && m>50) || (h == 19 && m<6)) {
				txx = "7:00 Minyan - Back Synagogue"		}
			else if((h == 19 && m>5) && (h == 19 && m<21)) {
				txx = "7:15 Minyan - Midrash"		}
			else if((h == 19 && m>20) && (h == 19 && m<50)) {
				txx = "7:30 Minyan - Back Synagogue"		}		
		}
	}
	
	if(txx != tempmsg) {
		txx = "Closest Minyan: <b>"+txx+"</B>";
	}
	return txx;
}


   var http_request = false;
   function makeRequest(url, parameters) {
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         return false;
      }
      http_request.onreadystatechange = alertContents;
      http_request.open('GET', url + parameters, true);
      http_request.send(null);
   }

   function alertContents() {
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {
            var xmldoc = http_request.responseText;   
			document.getElementById("weather").innerHTML = xmldoc;
         } else {
            //alert(http_request.status);
         }
      }
   }
   function getURL(turl, tvars) {
      makeRequest(turl, tvars);
   }
getURL("http://www.dealshul.com/display/yweather.asp","");

// -->
</script>

<div id="successor" style="display:none;">
!SUCCESS!
</div>

</body>
</html>
