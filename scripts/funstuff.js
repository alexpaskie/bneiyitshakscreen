

function hideSprites() {
	document.getElementById("peeker").src="../images/circle3.png"
	document.getElementById("blinky").innerHTML="&nbsp;"
}

function moveCircs() {
	hideSprites();
	document.getElementById("circ4img").src="../images/circ4full.gif"
	document.getElementById("contactform").style.display = "";
	var managerx = new jsAnimManager(20);  
	var animx1 = managerx.createAnimObject("circ3");  
	var animx2 = managerx.createAnimObject("circ4");  
	var animx = managerx.createAnimObject("circ2");  
	animx.add({property: Prop.wait, duration: 10});
	animx.add({property:  Prop.left, to: -250, ease: jsAnimEase.backout(0.6), duration: 400});
	animx1.add({property: Prop.wait, duration: 100});
	animx1.add({property: Prop.left, to: -500, ease: jsAnimEase.backout(0.6), duration: 500});
	animx2.add({property: Prop.wait, duration: 200});
	animx2.add({property: Prop.left, to: -750, ease: jsAnimEase.backout(0.6), duration: 600, onComplete: showContact});	
}



function sendMail() {
	urlx="emailer.asp?e="+document.getElementById("email").value+"&p="+document.getElementById("phone").value+"&n="+document.getElementById("namer").value
	makeRequest(urlx,"")
}

function goodSend() {
	document.getElementById("contactform").style.display = "none";
	alert("Thanks for contacting Blink! A representative will contact you shortly.");
	var managerx = new jsAnimManager(20);  
	var animx = managerx.createAnimObject("circ2");  
	animx.add({property:  Prop.left, to: 0, ease: jsAnimEase.backout(0.6), duration: 700});
	var animx1 = managerx.createAnimObject("circ3");  
	animx1.add({property: Prop.left, to: 0, ease: jsAnimEase.backout(0.4), duration: 800});
	var animx2 = managerx.createAnimObject("circ4");  
	animx2.add({property: Prop.left, to: 0, ease: jsAnimEase.backout(0.2), duration: 900, onComplete: showHalfcirc});		
}

function showHalfcirc() {
	document.getElementById("circ4img").src="../images/circle4.gif";

}

function showContact() {

}

function startAnim() {
	document.getElementById("blinky").style.display="";
	var manager = new jsAnimManager(20);  
	var anim = manager.createAnimObject("blinky");  
	anim.add({property: Prop.left, to: 500, ease: jsAnimEase.parabolicNeg, duration: 2000, onComplete: faceFront});
	}


function faceFront() {
	var manager = new jsAnimManager(20);  
	var anim = manager.createAnimObject("blinky");  
	document.getElementById('blinkyimg').src='../images/blinkT.gif'
	setTimeout("moonWalk()",2000)
	
}

function moonWalk() {
	var manager = new jsAnimManager(20);  
	var anim = manager.createAnimObject("blinky");  
	document.getElementById('blinkyimg').src='../images/swaggerleftT.gif'
	anim.add({property: Prop.left, to: 110, ease: jsAnimEase.parabolicNeg, duration: 2000, onComplete: faceFront2});
}

function faceFront2() {
	document.getElementById('blinkyimg').src='images/blink.gif'

	
}


   var http_request = false;
   function makeRequest(url, parameters) {
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         return false;
      }
      http_request.onreadystatechange = alertContents;
      http_request.open('GET', url + parameters, true);
      http_request.send(null);
   }

   function alertContents() {
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {
            var xmldoc = http_request.responseText;  
			if(xmldoc == "e") {
				alert("Email address is invalid.  Please fix it.");
				document.getElementById("email").style.borderColor = "#CC0000";
			}
			else {
				goodSend();
			}
         } else {
            alert('There was a problem with the request.');
         }
      }
   }
   function getURL(turl, tvars) {
      makeRequest(turl, tvars);
   }

function startMJ() {
	hideSprites();
	document.getElementById("mjfloor").style.display="";
	document.getElementById("mjwalker").style.display="";
	document.getElementById("mj").src="mj.asp";
	var managerxx = new jsAnimManager(20);  
	var animx4 = managerxx.createAnimObject("mjwalker");  
	animx4.add({property: Prop.left, to: -1000, ease: jsAnimEase.backout(0.2), duration: 8000, onComplete: cutMJ });	
	//document.getElementById("mjwalker").style.display="none";	
}

function cutMJ() {
	document.getElementById("mj").src="blank.asp";
	var managerxxx = new jsAnimManager(20);  
	var animx4 = managerxxx.createAnimObject("mjwalker");  
	animx4.add({property: Prop.left, to: 0, duration:10 });
	document.getElementById("mjwalker").style.display="none";
	document.getElementById("mjfloor").style.display="none";	
}

