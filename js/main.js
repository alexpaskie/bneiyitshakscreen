
trecs = "";
tsecs = "";
currslide = 0;
stopslides = 0;
delayslides = 0;
animstyles = "";

var dorefresh = 0;


$(document).ready(function () {

    updateClock();
    updataAjax();
    updataDaily();
    refreshPage();
    }
);


function randomUpto() {
    return Math.floor((Math.random() * 1000000) + 1);
}




function loadSlideVals() {
    delayslides = 12000;
    trecs = $("#sliderecs").html();
    trecs = trecs.split(",");
    tsecs = $("#slidetimes").html();
    tsecs = tsecs.split(",");
    animstyles = $("#animstyles").html();
    animstyles = animstyles.split(",");
    currslide = 0;
    stopslides = 0;
    slideIn();
}

function slideIn() {
    if (stopslides != 0) { return; }
    if (currslide > trecs.length - 1) { currslide = 0 };
    var displayslide = $("#s_" + trecs[currslide]);

    if (animstyles[currslide].indexOf("100002") > -1) {
        fadeIn(displayslide);
    }
    else if (animstyles[currslide].indexOf("100000") > -1) {
        slideInRight(displayslide);
    }
    else {
        slideInLeft(displayslide);
    }
}


function delayOut(numsecs) {
    if (stopslides == 0) { setTimeout("slideOut()", tsecs[currslide]); }

}

function slideOut() {
    var displayslide = $("#s_" + trecs[currslide]);

    if (animstyles[currslide].indexOf("100002") > -1) {
        fadeOut(displayslide);
    }
    else if (animstyles[currslide].indexOf("100000") > -1) {
        slideOutRight(displayslide);
    }
    else {
        slideOutLeft(displayslide);
    }
}


function slideInLeft(slide) {
    slide.animate({ left: 390 }, 1000, function () { delayOut(2000) });
}

function slideOutLeft(slide) {
    slide.animate({ left: -1200 }, 1000, function () { continueSlides(slide); });
}


function slideInRight(slide) {
    slide.css("left", "-2000px");
    slide.animate({ left: 390 }, 1000, function () { delayOut(2000) });
}

function slideOutRight(slide) {
    slide.animate({ left: 2000 }, 1000, function () { continueSlides(slide) });
}


function fadeIn(slide) {
    slide.hide();
    slide.css("left", "390px");
    slide.fadeIn(1000, "swing", function () { delayOut(2000) });
}

function fadeOut(slide) {
    slide.fadeOut(1000, "swing", function () { continueSlides(slide) });
}


function continueSlides(slide) {
    slide.css("left", "2000px");
    slide.show();
    currslide++;
    if (stopslides == 0) { slideIn() }
}


function updateClock ( )
{
  var currentTime = new Date ( );

  var currentHours = currentTime.getHours ( );
  var milHours = currentTime.getHours ( );
  var currentMinutes = currentTime.getMinutes ( );
  var currentSeconds = currentTime.getSeconds ( );
  var thisDay=currentTime.getDay()
  var milMins = currentTime.getMinutes ( );

  // Pad the minutes and seconds with leading zeros, if required
  currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  // Choose either "AM" or "PM" as appropriate
  var timeOfDay = ( currentHours < 12 ) ? "<span style='font-size:32px;'>AM</span>" : "<span style='font-size:32px;'>PM</span>";

  // Convert the hours component to 12-hour format if needed
  currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  // Convert an hours component of "0" to "12"
  currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  // Compose the string for display
  var currentTimeString = currentHours + ":" + currentMinutes + " " + timeOfDay;

  // Update the time display
  document.getElementById("timenow").innerHTML = currentTimeString;
  minyanUpdate(milHours, milMins, thisDay);
  
  dorefresh++;
  if(milHours == 0 && milMins == 10) {
  if(dorefresh>3) {
  	window.location.href=thiswebsite+"displaycontent.asp?doref="+randomUpto();
  }
  }
  
  
  setTimeout("updateClock()",30000);

}






function updataDaily() {
    $("#all_slides").html("");
    stopslides = 1;

    minutesx = 10
    mindelay = minutesx * 60 * 1000
    //mindelay = 30000

    $.ajax({ url: 'zmanim.asp', success: function (data) {

        $("#zmantimes").html(data)
    }

    });


    $.ajax({ url: 'yweather.asp', success: function (data) {
        tdata = data.replace("Forecast:", "Forecast:");
        tdata = tdata.replace("Wed - ", "<br><b>Wednesday</b><br>");
        tdata = tdata.replace("Sun - ", "<br><b>Sunday</b><br>");
        tdata = tdata.replace("Mon - ", "<br><b>Monday</b><br>");
        tdata = tdata.replace("Tue - ", "<br><b>Tuesday</b><br>");
        tdata = tdata.replace("Thu - ", "<br><b>Thursday</b><br>");
        tdata = tdata.replace("Fri - ", "<br><b>Friday</b><br>");
        tdata = tdata.replace("Sat - ", "<br><b>Saturday</b><br>");
        $("#weather").html(tdata)
    }

    });



    $.ajax({ url: 'slides.asp?ex=1', success: function (data) {
        $("#all_slides").html(data)
        setTimeout("loadSlideVals()", delayslides);
    }

    });

    setTimeout("updataDaily()", mindelay);

}



function refreshPage() {
    $.ajax({ url: 'dorefresh.asp', success: function (data) {
        if (data == "YES") {
            window.location.href = thiswebsite + "displaycontent.asp?doref=" + randomUpto();
        }
    }

    });

    $.ajax({ url: thiswebsite + '/hebcal.asp?doref=' + randomUpto(), success: function (data) {
        if (data.length > 50) {
            $("#recent_news").html(data);
        }
    }

    });

    setTimeout("refreshPage()", 10000);

}


function updataAjax() {



    //$.ajax({ url:'yahoonews.asp', success: function(data) { 
    //		$("#recent_news").html(data) 
    //		} 

    //		});		

    $.ajax({ url: 'traffic.asp', success: function (data) {
        $("#s_live_traffic").html(data)
    }

    });

    setTimeout("updataAjax()", 300000);
}
