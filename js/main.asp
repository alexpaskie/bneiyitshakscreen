<!--#include virtual="establish.asp"-->
<% minyan_info = getMinyanInfo	%>


function minyanUpdate(milHours, milMins, thisDay ) {

  var lastMinyan = "";
  var currentMinyan = "";

<% 
	breakt = split(minyan_info,VbCrLf)
	for x=0 to ubound(breakt) 
		ampm=0
		if len(breakt(x))<6 then exit for
		'response.Write breakt(x) 
		bt=split(breakt(x),"|") 	
		if instr(lcase(bt(0)),"pm")>0 then ampm=12
		minyan_time=replace(replace(bt(0),"pm",""),"am","")
		btx = split(minyan_time,":")
		minyan_hour=btx(0)+ampm:minyan_minute=trim(btx(1))
		%>
		
		if(<%=minyan_hour%> == milHours) {
			if(<%=int(minyan_minute)%>>=milMins) {
				currentMinyan = '<%=replace(replace(breakt(x),"|"," - "),"'","\'")%>'
				updateMinyanText(currentMinyan,lastMinyan);
				return;
			}
		}
		else if(<%=minyan_hour%>>milHours) {
				currentMinyan = '<%=replace(replace(breakt(x),"|"," - "),"'","\'")%>'
				updateMinyanText(currentMinyan,lastMinyan);
				return;
		}	
		lastMinyan = '<%=replace(replace(breakt(x),"|"," - "),"'","\'")%>';	
<%		
	next
%>
updateMinyanText(currentMinyan,lastMinyan);
	
	// alert(<%=int(minyan_minute)%>+"="+milMins)
}

function updateMinyanText(data1,data2) {
	$("#current_minyan").html(data1);
	$("#last_minyan").html(data2);
}