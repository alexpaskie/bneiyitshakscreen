<% 
pagename="minyanim.asp"
manager_name="Minyan Schedule"
db_name="minyanim"
db_field=""
%>
<!--#INCLUDE FILE="establish.asp"-->
<!--#include file="bsmanagerfunc.asp"-->
<!--#include file="sitedynamix.asp"-->
<!--#include file="pagedynamix.asp"-->
<%



'*************************************************
' Global Variables
'*************************************************


FlagHasPopupCalendar=True
rec_id=Request.QueryString("rec_id")					'identity
ident="rec_id"					'identity

'Field Desc, Type, MaxLen, Req/Optional/Manual, Appears in Listing, Fixvars Type
Set lstFN=Server.CreateObject("Scripting.Dictionary")
lstFN.Add "minyan_title","Schedule Title,varchar,200,required,list,0"
lstFN.Add "minyan_day","Days Applied,varchar,50,optional,list,0"
lstFN.Add "minyan_info","Minyan Schedule,varchar,4096,optional,nolist,0"
lstFN.Add "minyan_date","Specific For Date,datetime,8,optional,nolist,0"


'dynamic view/edit - add new fields made after manager maker page created
newfields=""
field_names=MakeFieldList
Set lstVN=Server.CreateObject("Scripting.Dictionary")

'------------------------------------
' Other Filters
'------------------------------------

'------------------------------------
' Pager
'------------------------------------
pagenum=Request.QueryString("pagenum")
numtoshow=25
if pagenum="" then pagenum=0


'*************************************************
' Any variables that need to span pages go here
' and are user preferences i.e. how to sort, etc.
'*************************************************
Function FollowPath(howtomove)
	FollowPath="pagenum=" & howtomove 
End Function

Sub PageHeader %>

<%
End Sub

'*************************************************
' Where to go on the page
'*************************************************

If Searchf="1" then
        Call ShowForm("Search")
ElseIf searchparams<>"" then
		call GetFormVars
		allownulls=true
		CheckForErrors=Errorthem()
		IF CheckForErrors="" then       
			Call GenerateSearchSQL
				response.redirect("minyanim.asp")
		Else
			Call ShowForm("Search")
		End If
elseIf shownothing="1" then          
		call listupdate
elseIf addrec=1  then
	call ShowForm("Add")
	
ElseIf viewrecord=1 THEN
	call ViewForm

ElseIf editrecord=1 THEN
	uploadimage=Request.QueryString("uploadimage")
	
	IF uploadimage<>"" then	
		session("db_name")=db_name%>
	<SCRIPT>
	  window.open("getfile.asp?thesku=<%=rec_id%>&dbname=<%=db_name%>&db_field=<%=uploadimage%>",'upwin','width=417,height=300,directories=no,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,resizable=yes');
	  window.location.href=('minyanim.asp?editrecord=1&rec_id=<%=rec_id%>&<%=followpath(pagenum)%>')
	</SCRIPT><%
	End If
	call GetVarsDB
	If Not FlagNoRecord Then
		call ShowForm("Update")
	Else
		Response.redirect("minyanim.asp?" & followpath(pagenum))
	End If
	
ElseIf addform<>"" THEN
	'------------------------------------
	' happens after clicking "add" button
	'------------------------------------
	call GetFormVars
	CheckForErrors=Errorthem()
	IF CheckForErrors="" then 
		cmdok=sqlInsert(db_name, field_names, "vtype=lst")
		uploadimg=Request.Form("uploadimg"):if uploadimg<>"" then uploadimg=db_field
		if anotheradd<>"" then
			response.redirect("minyanim.asp?addrec=1&" & followpath(pagenum))
		else
			set rs=server.createobject("adodb.recordset")
			sql="SELECT * FROM minyanim order by rec_id desc"
			rs.open sql,conn
			rec_id=rs("rec_id")
			response.redirect("minyanim.asp?editrecord=1&uploadimage=" & uploadimg & "&rec_id=" & rec_id & "&" & followpath(pagenum))
		end if
	Else
		call ShowForm("Add")
	End If

ElseIf updateform<>"" and deleteform="" THEN
	'------------------------------------
	' happens after clicking "update" button
	'------------------------------------
	call getformvars
	CheckForErrors=Errorthem()
	IF CheckForErrors="" then 
		cmdok=sqlUpdate(db_name, field_names, "vtype=lst", ident & "=" & eval(ident))
		uploadimg=Request.Form("uploadimg"):if uploadimg<>"" then uploadimg=db_field
		response.redirect("minyanim.asp?editrecord=1&uploadimage=" & uploadimg & "&rec_id=" & rec_id & "&" & followpath(pagenum))
	Else
		call ShowForm("Update")
	End If

ElseIf deleteform<>"" or confirmdelete<>"" or deletefromlist<>"" THEN
	IF confirmdelete="" and deletefromlist="" then GetFormVars
	deleterecord=sqlDelete(db_name, conn, " " & ident & "=" & eval(ident))
	response.redirect("minyanim.asp?" & followpath(pagenum))


Else
	Response.Cookies(db_name)("advsearch")=""
	NoJavaForm=True
	call ShowRecordList
End If

LoadHeaderClose





'******************************************************************************************************
'******************************************************************************************************

'LLLLLLL                   LLLLLLLLLLLLLLLLLLLLL       LLLLLLLLLLLLLLLLLLL    LLLLLLLLLLLLLLLLLLLLLLLLL
'LLLLLLL                   LLLLLLLLLLLLLLLLLLLLL      LLLLLLLLLLLLLLLLLLLL    LLLLLLLLLLLLLLLLLLLLLLLLL
'LLLLLLL                          LLLLLLL            LLLLLLL                           LLLLLLL       
'LLLLLLL                          LLLLLLL             LLLLLL                           LLLLLLL       
'LLLLLLL                          LLLLLLL              LLLLLLLLLLLLLLLLL               LLLLLLL       
'LLLLLLL                          LLLLLLL                          LLLLLL              LLLLLLL       
'LLLLLLL                          LLLLLLL                          LLLLLLL             LLLLLLL       
'LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLLL              LLLLLLL       
'LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLL               LLLLLLL       

'****************************************************************************************************
'****************************************************************************************************

'*************************************************
' Listing of all records
'*************************************************


Sub ShowRecordList()
	Set rs=Server.CreateObject("ADODB.Recordset")
	sql=GetSqlShowAll()
	rs.Open sql,conn
	IF pagenum>0 then rs.Move (numtoshow*pagenum)
	
	totalrecords=GetTotalRecords(sql) 
	Call LoadHeader
	Call PageHeader%>
	<%=ConfirmLoseChanges%>
	<table bgcolor="<%=FilterTBThinLineBorder%>" cellspacing="1" cellpadding="0" width="100%">
	<tr>
	<td  bgcolor="<%=FilterTbBg%>">
	<form name="filterform" action="minyanim.asp" method="get" >
        <table cellpadding="0" cellspacing="0">
		<tr>
                <td width="300" bgcolor="<%=FilterTbBg%>"><%=ShowButton("Add A New Record",0,"minyanim.asp?addrec=1&" & followpath(pagenum),250)%></td>
                <td width="1" bgcolor="<%=FilterTBThinLineBorder%>"><img src="images/spacer.gif" width="1"></td>
				<td width="*" bgcolor="<%=FilterTbBg%>"></td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
                <td align="right" width="200" bgcolor="<%=FilterTbBg%>">&nbsp;Search For&nbsp;</td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
				<td  bgcolor="<%=FilterTbBg%>"><input type="text" name="filtername" value="<%=filtername%>" style="font-size:9px;" class="filter_box"></td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
                <td  bgcolor="<%=FilterTbBg%>">&nbsp;in&nbsp;</td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
                <td bgcolor="<%=FilterTbBg%>"><select name="filterby" class="filter_box">



                <option value="minyanim.minyan_title" <%=iif(filterby="minyanim.minyan_title","SELECTED","")%>>Schedule Title</option><br>
                <option value="minyanim.minyan_day" <%=iif(filterby="minyanim.minyan_day","SELECTED","")%>>Days Applied</option><br>


                   </select></td>
                      
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
		<td  bgcolor="<%=FilterTbBg%>"><input type="image" img src="images/but_filter_<%=iif(filtername<>"","on","off")%>.gif" ></td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>

		</tr>
		</table>
	</td>
	</tr>
	</table>
	<table align="center">
	<tr>
	<td align="center">
		<table>
		<tr>
		<td>
		<%=ShowButton("All",iif(filtername="" and advsearch="",1,0),"minyanim.asp?" & followpath(0) & "&filtername=",30)%></td>
<%	For X=65 to 90 
		theletter=CHR(x)%>
		<td>
		<%=ShowButton(theletter,iif(filtername=theletter,1,0),"minyanim.asp?" & followpath(0) & "&filtername=" & theletter & "&showrecs=1",10)%>
		</td>		
<% 		 if theletter="P" then response.write "</tr><tr><td colspan=""3"">"
 	NEXT %></form>
		<td colspan="2"><a href="minyanim.asp?searchf=1"><%=ShowButton("Search",iif(advsearch<>"",1,0),"minyanim.asp?" & followpath(0) & "&searchf=1",30)%></td>
		</tr>
		</table>
	</td>
	</tr>
	</table>		
	<%=ListUpdateJavascript%>
	<form name="theform">
	<input type="hidden" name="changed" value="0">
	<input type="hidden" name="changedf" value="">	



	<TABLE BORDER=0 WIDTH="100%" bgcolor="<%=ListTBThinLineBorder%>" cellpadding="1" cellspacing="1">
	<TR>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center">View</TD>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center">Edit</TD>
	<TD BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('minyanim.asp?sortorder=minyanim.minyan_title<%=IIF(sortorder="minyanim.minyan_title","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("minyan_title")%></TD>
	<TD BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('minyanim.asp?sortorder=minyanim.minyan_day<%=IIF(sortorder="minyanim.minyan_day","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("minyan_day")%></TD>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand">Save</TD>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand">Delete</TD>
	</TR>
	    <% IF rs.EOF THEN Response.Write "<TR><TD COLSPAN=""5""><B> (!) No Records To Show </B></td></tr>" %>
<%	Do Until rs.EOF 
		 %>
		<TR bgcolor="<%=ListTbBg%>"  onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand">
		<TD width="5%" align="center" onClick="window.location.href=('minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1')">
		<img src="images/but_view.gif" border="0" name="e_img_minyan_title">
		<TD width="5%" align="center" onClick="window.location.href=('minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&editrecord=1')">
		<img src="images/pencil.gif" border="0" name="e_img_minyan_title">
		</td>
		<TD onClick="window.location.href=('minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1')" width="46%">
		<A HREF="minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1">
		<%=rs("minyan_title")%>
		</A>
		</td>
		<TD onClick="window.location.href=('minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1')" width="49%">
		<A HREF="minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1">
		<%
		bx = split(rs("minyan_day") & "",",")
		for x = 0 to ubound(bx) 
			response.Write(weekdayname(bx(x)+1)) & ","
		next%>
		</A>
		</td>
		<TD width="5%" align="center" onClick="dolistact('<%=rs("rec_id")%>','');">
		<img src="images/but_saved_yes.gif" name="img_<%=rs("rec_id")%>" border="0">
		</td>
		<TD width="5%" align="center" onClick="deleterec('<%=rs("rec_id")%>');">
		<img src="images/trash.gif" border="0">
		</td>


<%		numrecs=numrecs+1
		rs.Movenext
		If numrecs = numtoshow and NOT rs.EOF then
			flagnext=True
			exit do
		End If 
	Loop %>
	</table>
	<TABLE WIDTH="80%" align="center">
	<TR>
<%	IF pagenum>0 THEN %>
		<A HREF="minyanim.asp?<%=followpath(pagenum-1)%>">Previous Page</A>&nbsp;&nbsp;&nbsp;
<%	End If

	
	If totalrecords>numtoshow THEN %>
		&nbsp;&nbsp;
<%		pagerecs=INT(totalrecords/numtoshow)
		if totalrecords mod numtoshow>0 then pagerecs=pagerecs+1
		 %>
         Page: <input type="text" size="3" id="pagenumx" value="<%=pagenum+1%>" /> of <B><%=pagerecs%> 
         <input type="button" onClick="window.location.href='<%=minyanim%>?pagenum='+(document.getElementById('pagenumx').value-1)" value="Go" />
		
<%	IF flagnext THEN  %>
	&nbsp;&nbsp;	<A HREF="minyanim.asp?<%=followpath(pagenum+1)%>">Next Page</A>
<%	End If	

        End If
	rs.close
	Set rs=Nothing %>
	</TD></TR></TABLE>
</form>
<% End Sub








'*****************************************************************************************************************
'*****************************************************************************************************************

'VVVVVVV               VVVVVVV     VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV     VVVVVVV             VVVVVVV
' VVVVVVV             VVVVVVV      VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV     VVVVVVV             VVVVVVV
'  VVVVVVV           VVVVVVV              VVVVVVV            VVVVVVV                   VVVVVVV             VVVVVVV
'   VVVVVVV         VVVVVVV               VVVVVVV            VVVVVVVVVVVVVVVVV         VVVVVVV   VVVVVVV   VVVVVVV
'    VVVVVVV       VVVVVVV                VVVVVVV            VVVVVVVVVVVVVVVVV         VVVVVVV   VVVVVVV   VVVVVVV
'     VVVVVVV     VVVVVVV                 VVVVVVV            VVVVVVV                   VVVVVVV   VVVVVVV   VVVVVVV
'      VVVVVVV   VVVVVVV                  VVVVVVV            VVVVVVV                   VVVVVVV   VVVVVVV   VVVVVVV
'       VVVVVVV VVVVVVV            VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV      VVVVVVVVVVV   VVVVVVVVVVV
'        VVVVVVVVVVVVVV            VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV      VVVVVVVVVVV   VVVVVVVVVVV

'*****************************************************************************************************************
'*****************************************************************************************************************

'*************************************************
' View Record Area
'*************************************************

Sub ViewForm()
	Call GetVarsDB 
	Call LoadHeader
	Call PageHeader	%>
	<%=ConfirmLoseChanges%>

	<table bgcolor="<%=ViewTbThinLineBorder%>" cellspacing="1" cellpadding="0" width="100%" style="cursor:pointer;cursor:hand">
	<tr>
	<td  bgcolor="<%=ViewTbBg%>">
		<table cellpadding="0" cellspacing="0" width="100%">
		<tr >
		<td onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ViewTbBg%>'" width="50%" onClick="window.location.href=('minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>')" align="center" height="20"><A HREF="minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>" class=""tbl_header"">Back To Listing Page</A></td>
		<td width="1" bgcolor="<%=ViewTbThinLineBorder%>"><img src="images/spacer.gif" width="1"></td>
		<td width="50%" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ViewTbBg%>'" bgcolor="<%=ViewTbBg%>" align="center" onClick="window.location.href=('minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>&editrecord=1')" height="20"><A HREF="minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>&editrecord=1">Edit This Record</A></td>
		</tr>
		</table>
	</td>
	</tr>
	</table><img src="images/spacer.gif" height="5"><br>
    
	<TABLE BORDER=0 WIDTH="100%" bgcolor="<%=ViewTbThinLineBorder%>" cellpadding="1" cellspacing="1">
    <tr><td bgcolor="<%=ViewTbBg%>">
	<table width="80%" align="center">	



	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_title")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("minyan_title"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_day")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("minyan_day"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_info")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("minyan_info"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_date")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("minyan_date"))%></td>
	</TR>

	<%	IF newfields<>"" then Response.Write DrawNewFieldView %>	
	</table>
</td></tr></table>
<%	
End Sub











'********************************************************************************************************
'********************************************************************************************************

'EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEEEEEEE
'EEEEEEEEEEEEEEEEEEEEE     EEEEEEE        EEEEEEE    EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEEEEEEE
'EEEEEEE                   EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEEEEEEEEEEEE         EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEEEEEEEEEEEE         EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEE                   EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEE                   EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEEEEEEEEEEEEEEEE     EEEEEEE        EEEEEEE    EEEEEEEEEEEEEEEEEEEEE               EEEEEEE       
'EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE               EEEEEEE       

'********************************************************************************************************
'********************************************************************************************************





'*************************************************
'Form Area
'*************************************************




Sub ShowForm(FormType)		
		'*************************************************
		' Chose a Record To Edit, Or Add New
		'************************************************* 
		formact=formtype
		Call LoadHeader
		Call PageHeader %>
		<%=ConfirmLoseChanges%>
		<%=SaveFuncJavascript%>		
<%		Call DrawErrorthem
		IF CheckForErrors<>"" THEN %>		
			<TABLE BORDER=0 align="center" ><TR><td valign="middle"><img src="images/alert.gif"></td><td><span style="color:#930000; font-size:14px;"><B>Please Correct The Following Errors: </font></b></td></tr><td></td><td><ul><%=CheckForErrors%></ul></td></tr></table>
<%		END IF 
		IF flagHasComboBox then Call DrawComboBox%>
		
	
		<form name="theform" action="minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>" method="POST" <% IF formtype<>"Search" then %>onSubmit="return ValidForm()"<% end if %>>
		<input type="hidden" name="changed" value="<%=IIF(checkforerrors="",0,1)%>">

        <TABLE BORDER=0 WIDTH="100%" bgcolor="<%=EditTbThinLineBorder%>" cellpadding="1" cellspacing="1">
		<tr><td bgcolor="<%=EditTbBg%>" align="center">       
		<% If FormType="Search" THEN %><B>To search for records, fill out all or part of any or all fields below and click "Search".</b><% end If %>

		<table width="80%" align="center">
		<TR><TD></TD><TD>
<%		IF FormType="Add" THEN %>
			<input type="hidden" name="addform" value="Add">
			<input type="Submit" onClick="return oksubmit();" name="savebutton" value="Save" >
			<input type="Submit" onClick="return oksubmit();" name="anotheradd" value="Add Another Record" >
<%		ElseIf FormType="Update" THEN %>			
			<input type="hidden" name="updateform" value="Update">
			<input type="Submit" onClick="return oksubmit();" name="savebutton" value="Save" >
			<input type="button" onClick="deleterec('<%=rec_id%>');" name="deleteform" value="Delete">
<%		ElseIf FormType="Search" THEN %>                        
			<input type="hidden" name="searchparams" value="Search">
			<input type="Submit" onClick="return oksubmit();" value="Search">
<%		End IF %>
		<input type="button" value="Back to Listing" onClick="window.location='minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>';">
		</TD></TR>	



		<INPUT TYPE="HIDDEN" NAME="rec_id" value="<%=FixWords(rec_id)%>">
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_title")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="75%"><INPUT TYPE="TEXT" NAME="minyan_title" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("minyan_title"))%>" size=55 <%=stylescript%>></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_day")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="75%">
        <% for x=0 to 6 %>
        <li style="float:left; width:200px;"><INPUT TYPE="checkbox" NAME="minyan_day" value="<%=x%>" <%=IIF(instr(lstVN("minyan_day"),x)>0,"CHECKED","")%>><%=weekdayname(x+1)%></li>
        <% next %>
        </TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_info")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="75%"><textarea NAME="minyan_info" cols=50 rows=30 onKeyup="changesavebutton();" onChange="addchange();"  <%=stylescript%>><%=FixWords(lstVN("minyan_info"))%></textarea></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("minyan_date")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="75%"><INPUT TYPE="TEXT" NAME="minyan_date" value="<%=FixWords(lstVN("minyan_date"))%>" size="10" onKeyup="changesavebutton();" onChange="javascript:addchange();" onKeydown="return dFilter (event.keyCode, this, '##/##/####');"  <%=stylescript%>><img src="popupcalendar/calendaricon.gif" alt="Choose Date" border=0 onClick="changesavebutton();popUpCalendar(this, document.all.minyan_date, 'mm/dd/yyyy', 0, 0); " ></TD>		</tr>

<%		IF newfields<>"" then DrawNewFieldEdit %>
		<TR><TD></TD><TD>
<%		IF FormType="Add" THEN %>
			<input type="hidden" name="addform" value="Add">
			<input type="Submit" onClick="return oksubmit();" name="savebutton2" value="Save" >
			<input type="Submit" onClick="return oksubmit();" name="anotheradd2" value="Add Another Record" >
<%		ElseIf FormType="Update" THEN %>			
			<input type="hidden" name="updateform" value="Update">
			<input type="Submit" onClick="return oksubmit();" name="savebutton2" value="Save" >
			<input type="button" onClick="deleterec('<%=rec_id%>');" name="deleteform" value="Delete">
<%		ElseIf FormType="Search" THEN %>                        
			<input type="hidden" name="searchparams" value="Search">
			<input type="Submit" onClick="return oksubmit();" value="Search">
<%		End IF %>
		<input type="button" value="Back to Listing" onClick="window.location='minyanim.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>';">
		</td></tr></table></td></tr></table>	
		</form>

<%
End Sub
%>





















<%



'**************************************************************************************************
'**************************************************************************************************
'*************************************************
' End of HTML Writing to the page
'*************************************************
'**************************************************************************************************
'**************************************************************************************************






















	
'**************************************************************************************************
'**************************************************************************************************

'OOOOOOOOOOO  OOOO   OOOO  OOOO   OOOO   OOOOOOOOO   OOOOOOOOOOOO   OOO    OOOOOOOOO   OOOO   OOOO   OOOOOOOOO
'OOOO         OOOO   OOOO  OOOOOO OOOO  OOOOO  OOOO  OOOOOOOOOOOO  OOOOO  OOOOOOOOOOO  OOOOOO OOOO  OOOOO    OO
'OOOOOOOO     OOOO   OOOO  OOOOOOOOOOO  OOOO             OOOO      OOOOO  OOO     OOO  OOOOOOOOOOO  OOOOOO
'OOOOOOOO     OOOO   OOOO  OOOOOOOOOOO  OOOO             OOOO      OOOOO  OOO     OOO  OOOOOOOOOOO     OOOOOOOO
'OOOO         OOOOOOOOOOO  OOOO  OOOOO  OOOOO  OOOO      OOOO      OOOOO  OOOOOOOOOOO  OOOO  OOOOO  OO    OOOOO
'OOOO          OOOOOOOOO   OOOO   OOOO   OOOOOOOOO       OOOO       OOO    OOOOOOOOO   OOOO   OOOO   OOOOOOOOO


'8888   8888   888888888   8888888888    8888888888
'8888   8888  8888   8888  8888   8888  88888     88
'8888   8888  8888   8888  8888   8888   88888888
'88888 88888  88888888888  8888888888      88888888
' 888888888   8888   8888  8888 888888  88     88888
'   88888     8888   8888  8888   8888   8888888888

'**************************************************************************************************
'**************************************************************************************************
' Subs to Get variables from form, db, 
' full recordset list	sql=Replace(sql,"SELECT * FROM","SELECT COUNT(*) AS numrecs FROM")

Sub GetVarsDb()
	Set rs=Server.CreateObject("ADODB.Recordset")


	sql="SELECT minyanim.*" &_
		" FROM minyanim " &_ 
		" WHERE (minyanim.rec_id=" & rec_id & ")"


	rs.Open sql,conn
	If Not rs.EOF Then
		for each fname in rs.Fields
			lstVN.Add FormReady(fname.name), FormReady(fname)
		Next
	Else
		FlagNoRecord=True
	End If
	
	rs.Close
	Set rs=Nothing
End Sub

Function GetSqlShowAll()

	IF sortorder<>"" then 
		sortorder1=Replace(sortorder,"-"," ")
	else
		sortorder1="minyanim.minyan_title"
	End If

	sql="SELECT minyanim.*" &_
		" FROM minyanim " &_ 
		" WHERE (minyanim.rec_id IS NOT NULL) :1 ORDER BY " & sortorder1

	IF filtername<>"" THEN
        if filterby="" then filterby="minyanim.minyan_title"
		sql=Replace(sql,":1"," AND " & filterby & " LIKE '" & replace(filtername,"'","''") & "%' :1")
   	End IF
	sql=replace(sql,":1","")

	IF advsearch<>"" then sql=advsearch
	sql=Replace(sql,":1","")
	GetSqlShowAll=sql
End Function


Function ErrorthemManual()
	'Put your manual Error checking in here...
	
	'IF lstV("field")<>"" THEN 
	'	If len(lstV("field"))<1  and not allownulls THEN 
	'		ErrorthemManual=ErrorthemManual & "<LI>" & FieldDesc("field") & " can't be less than 3 characters long."
	'	ElseIf len(lstV("field"))>50 THEN 
	'		ErrorthemManual=ErrorthemManual & "<LI>" & FieldDesc("field") & " can't be more than 50 characters long."
	'	END IF
	'End If 

End Function

%>
