<% 
pagename="slides.asp"
manager_name="Slide Manager"
db_name="slides"
db_field="content_img_1"
%>
<!--#INCLUDE FILE="establish.asp"-->
<!--#include file="bsmanagerfunc.asp"-->
<!--#include file="sitedynamix.asp"-->
<!--#include file="pagedynamix.asp"-->
<%



'*************************************************
' Global Variables
'*************************************************


'FlagHasPopupCalendar=True
rec_id=Request.QueryString("rec_id")					'identity
ident="rec_id"					'identity

'Field Desc, Type, MaxLen, Req/Optional/Manual, Appears in Listing, Fixvars Type
Set lstFN=Server.CreateObject("Scripting.Dictionary")
lstFN.Add "slide_title","Slide Title,varchar,50,required,list,0"
lstFN.Add "display_order","Display Order,int,4,required,list,1"
lstFN.Add "station_id","Station,varchar,50,optional,list,0"
lstFN.Add "days_active","Days Active,varchar,50,optional,list,0"
lstFN.Add "design_style","Design Style,int,4,required,nolist,1"
lstFN.Add "content_top","Content Top,varchar,2000,optional,nolist,0"
lstFN.Add "content_middle","Content Middle,varchar,8000,optional,nolist,0"
lstFN.Add "content_bottom","Content Bottom,varchar,2000,optional,nolist,0"
lstFN.Add "content_img_1","Content Img 1,varchar,50,optional,nolist,0"
lstFN.Add "content_img_2","Content Img 2,varchar,50,optional,nolist,0"
lstFN.Add "display_time","Display Time (Seconds),int,4,optional,list,1"
lstFN.Add "animation_style","Animation Style,int,4,optional,nolist,1"
lstFN.Add "start_on","Start On,datetime,8,optional,nolist,0"
lstFN.Add "end_on","End On,datetime,8,optional,nolist,0"
lstFN.Add "start_time","Start Time,datetime,8,optional,nolist,0"
lstFN.Add "end_time","End Time,datetime,8,optional,nolist,0"
lstFN.Add "is_live","Is Live,int,4,optional,list,2"

'dynamic view/edit - add new fields made after manager maker page created
newfields=""
field_names=MakeFieldList
Set lstVN=Server.CreateObject("Scripting.Dictionary")

'------------------------------------
' Other Filters
'------------------------------------

'------------------------------------
' Pager
'------------------------------------
pagenum=Request.QueryString("pagenum")
numtoshow=25
if pagenum="" then pagenum=0


'*************************************************
' Any variables that need to span pages go here
' and are user preferences i.e. how to sort, etc.
'*************************************************
Function FollowPath(howtomove)
	FollowPath="pagenum=" & howtomove 
End Function

Sub PageHeader %>

<%
End Sub

'*************************************************
' Where to go on the page
'*************************************************

If Searchf="1" then
        Call ShowForm("Search")
ElseIf searchparams<>"" then
		call GetFormVars
		allownulls=true
		CheckForErrors=Errorthem()
		IF CheckForErrors="" then       
			Call GenerateSearchSQL
				response.redirect("slides.asp")
		Else
			Call ShowForm("Search")
		End If
elseIf shownothing="1" then          
		call listupdate
elseIf addrec=1  then
	call ShowForm("Add")
	
ElseIf viewrecord=1 THEN
	call ViewForm

ElseIf editrecord=1 THEN
	uploadimage=Request.QueryString("uploadimage")
	
	IF uploadimage<>"" then	
		session("db_name")=db_name
		session("upload") = 1 
	End If
	call GetVarsDB
	If Not FlagNoRecord Then
		call ShowForm("Update")
	Else
		Response.redirect("slides.asp?" & followpath(pagenum))
	End If
	
ElseIf addform<>"" THEN
	'------------------------------------
	' happens after clicking "add" button
	'------------------------------------
	call GetFormVars
	CheckForErrors=Errorthem()
	IF CheckForErrors="" then 
		cmdok=sqlInsert(db_name, field_names, "vtype=lst")
		uploadimg=Request.Form("uploadimg"):if uploadimg<>"" then uploadimg=db_field
		if anotheradd<>"" then
			response.redirect("slides.asp?addrec=1&" & followpath(pagenum))
		else
			set rs=server.createobject("adodb.recordset")
			sql="SELECT * FROM slides order by rec_id desc"
			rs.open sql,conn
			rec_id=rs("rec_id")
			response.redirect("slides.asp?editrecord=1&uploadimage=" & uploadimg & "&rec_id=" & rec_id & "&" & followpath(pagenum))
		end if
	Else
		call ShowForm("Add")
	End If

ElseIf updateform<>"" and deleteform="" THEN
	'------------------------------------
	' happens after clicking "update" button
	'------------------------------------
	call getformvars
	CheckForErrors=Errorthem()
	IF CheckForErrors="" then 
		cmdok=sqlUpdate(db_name, field_names, "vtype=lst", ident & "=" & eval(ident))
		uploadimg=Request.Form("uploadimg"):if uploadimg<>"" then uploadimg=db_field
		response.redirect("slides.asp?editrecord=1&uploadimage=" & uploadimg & "&rec_id=" & rec_id & "&" & followpath(pagenum))
	Else
		call ShowForm("Update")
	End If

ElseIf deleteform<>"" or confirmdelete<>"" or deletefromlist<>"" THEN
	IF confirmdelete="" and deletefromlist="" then GetFormVars
	deleterecord=sqlDelete(db_name, conn, " " & ident & "=" & eval(ident))
	response.redirect("slides.asp?" & followpath(pagenum))


Else
	Response.Cookies(db_name)("advsearch")=""
	NoJavaForm=True
	call ShowRecordList
End If

LoadHeaderClose





'******************************************************************************************************
'******************************************************************************************************

'LLLLLLL                   LLLLLLLLLLLLLLLLLLLLL       LLLLLLLLLLLLLLLLLLL    LLLLLLLLLLLLLLLLLLLLLLLLL
'LLLLLLL                   LLLLLLLLLLLLLLLLLLLLL      LLLLLLLLLLLLLLLLLLLL    LLLLLLLLLLLLLLLLLLLLLLLLL
'LLLLLLL                          LLLLLLL            LLLLLLL                           LLLLLLL       
'LLLLLLL                          LLLLLLL             LLLLLL                           LLLLLLL       
'LLLLLLL                          LLLLLLL              LLLLLLLLLLLLLLLLL               LLLLLLL       
'LLLLLLL                          LLLLLLL                          LLLLLL              LLLLLLL       
'LLLLLLL                          LLLLLLL                          LLLLLLL             LLLLLLL       
'LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLLL              LLLLLLL       
'LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLLLL     LLLLLLLLLLLLLLLLLLL               LLLLLLL       

'****************************************************************************************************
'****************************************************************************************************

'*************************************************
' Listing of all records
'*************************************************


Sub ShowRecordList()
	Set rs=Server.CreateObject("ADODB.Recordset")
	sql=GetSqlShowAll()
	rs.Open sql,conn
	IF pagenum>0 then rs.Move (numtoshow*pagenum)
	
	totalrecords=GetTotalRecords(sql) 
	Call LoadHeader
	Call PageHeader%>
	<%=ConfirmLoseChanges%>
	<table bgcolor="<%=FilterTBThinLineBorder%>" cellspacing="1" cellpadding="0" width="100%">
	<tr>
	<td  bgcolor="<%=FilterTbBg%>">
    <div class="showonmobile"><%=ShowButton("Add A New Record",0,"slides.asp?addrec=1&" & followpath(pagenum),250)%></div>
	<form name="filterform" action="slides.asp" method="get" >
        <table cellpadding="0" cellspacing="0"  class="hideonmobile">
		<tr>
                <td width="300" bgcolor="<%=FilterTbBg%>"><%=ShowButton("Add A New Record",0,"slides.asp?addrec=1&" & followpath(pagenum),250)%></td>
                <td width="1" bgcolor="<%=FilterTBThinLineBorder%>"><img src="images/spacer.gif" width="1"></td>
				<td width="*" bgcolor="<%=FilterTbBg%>"></td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
                <td align="right" width="200" bgcolor="<%=FilterTbBg%>">&nbsp;Search For&nbsp;</td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
				<td  bgcolor="<%=FilterTbBg%>"><input type="text" name="filtername" value="<%=filtername%>" style="font-size:9px;" class="filter_box"></td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
                <td  bgcolor="<%=FilterTbBg%>">&nbsp;in&nbsp;</td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
                <td bgcolor="<%=FilterTbBg%>"><select name="filterby" class="filter_box">



                <option value="slides.slide_title" <%=iif(filterby="slides.slide_title","SELECTED","")%>>Slide Title</option><br>
                <option value="slides.display_order" <%=iif(filterby="slides.display_order","SELECTED","")%>>Display Order</option><br>
                <option value="slides.station_id" <%=iif(filterby="slides.station_id","SELECTED","")%>>Station</option><br>
                <option value="slides.display_time" <%=iif(filterby="slides.display_time","SELECTED","")%>>Display Time (Seconds)</option><br>


                   </select></td>
                      
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>
		<td  bgcolor="<%=FilterTbBg%>"><input type="image" img src="images/but_filter_<%=iif(filtername<>"","on","off")%>.gif" ></td>
                <td width="5" bgcolor="<%=FilterTbBg%>"><img src="images/spacer.gif" width="5"></td>

		</tr>
		</table>
	</td>
	</tr>
	</table>
	<table align="center"   class="hideonmobile">
	<tr>
	<td align="center">
		<table>
		<tr>
		<td>
		<%=ShowButton("All",iif(filtername="" and advsearch="",1,0),"slides.asp?" & followpath(0) & "&filtername=",30)%></td>
<%	For X=65 to 90 
		theletter=CHR(x)%>
		<td>
		<%=ShowButton(theletter,iif(filtername=theletter,1,0),"slides.asp?" & followpath(0) & "&filtername=" & theletter & "&showrecs=1",10)%>
		</td>		
<% 		 if theletter="P" then response.write "</tr><tr><td colspan=""3"">"
 	NEXT %></form>
		<td colspan="2"><a href="slides.asp?searchf=1"><%=ShowButton("Search",iif(advsearch<>"",1,0),"slides.asp?" & followpath(0) & "&searchf=1",30)%></td>
		</tr>
		</table>
	</td>
	</tr>
	</table>		
	<%=ListUpdateJavascript%>
	<form name="theform">
	<input type="hidden" name="changed" value="0">
	<input type="hidden" name="changedf" value="">	



<iframe src="slides.asp?shownothing=1" name="listupdater" width="0" height="0" ></iframe>

	<TABLE BORDER=0 WIDTH="100%" bgcolor="<%=ListTBThinLineBorder%>" cellpadding="1" cellspacing="1">
	<TR>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center">View</TD>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center">Edit</TD>

    <TD BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('slides.asp?sortorder=slides.is_live<%=IIF(sortorder="slides.is_live","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("is_live")%></TD>
    <TD BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('slides.asp?sortorder=slides.slide_title<%=IIF(sortorder="slides.slide_title","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("slide_title")%></TD>
	<TD BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('slides.asp?sortorder=slides.display_order<%=IIF(sortorder="slides.display_order","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("display_order")%></TD>
	<TD   class="hideonmobile" BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('slides.asp?sortorder=slides.station_id<%=IIF(sortorder="slides.station_id","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("station_id")%></TD>
    	<TD  class="hideonmobile" BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('slides.asp?sortorder=slides.days_active<%=IIF(sortorder="slides.days_active","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("days_active")%></TD>
	<TD BGCOLOR="<%=ListTbBg%>" align="middle" onClick="window.location.href=('slides.asp?sortorder=slides.display_time<%=IIF(sortorder="slides.display_time","-DESC","")%>')" class="tbl_header" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand"><%=FieldDesc("display_time")%></TD>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand">Save</TD>
	<TD BGCOLOR="<%=ListTbBg%>" class="tbl_header" align="center" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand">Delete</TD>
	</TR>
	    <% IF rs.EOF THEN Response.Write "<TR><TD COLSPAN=""16""><B> (!) No Records To Show </B></td></tr>" %>
<%	Do Until rs.EOF 
		 %>
		<TR bgcolor="<%=ListTbBg%>"  onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ListTbBg%>'" style="cursor:pointer;cursor:hand">
		<TD width="5%" align="center" onClick="window.location.href=('slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1')">
		<img src="images/but_view.gif" border="0" name="e_img_slide_title">
		<TD width="5%" align="center" onClick="window.location.href=('slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&editrecord=1')">
		<img src="images/pencil.gif" border="0" name="e_img_slide_title">
		</td>
                <TD width="3%">
		<input type="checkbox" name="f_<%=rs("rec_id") & "_is_live"%>" <%=IIF(rs("is_live") & ""="1","CHECKED","") %> value="1" onClick="changesaveimg('<%=rs("rec_id")%>');changedlistval('<%=rs("rec_id")%>')">
		</td>
		<TD onClick="window.location.href=('slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1')" width="43%">
		<A HREF="slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1">
		<%=rs("slide_title")%>
		</A>
		</td>

		<TD width="3%">
		<input type="text" name="f_<%=rs("rec_id") & "_display_order"%>" size="2" value="<%=rs("display_order")%>" onKeyup="changesaveimg('<%=rs("rec_id")%>')"  onChange="changedlistval('<%=rs("rec_id")%>')">
		</td>
		<TD  class="hideonmobile" onClick="window.location.href=('slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1')" width="23%">
		<A HREF="slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1">
		<%=rs("station_id")%>
		</A>
		</td>
		<TD  class="hideonmobile" onClick="window.location.href=('slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1')" width="23%">
		<A HREF="slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rs("rec_id")%>&viewrecord=1">
		<%
		bx = split(rs("days_active") & "",",")
		for x = 0 to ubound(bx) 
			response.Write(weekdayname(bx(x)+1)) & IIF(x<ubound(bx),",","")
		next%>        
		</A>
		</td>        
		<TD width="3%">
		<input type="text" name="f_<%=rs("rec_id") & "_display_time"%>" size="2" value="<%=rs("display_time")%>" onKeyup="changesaveimg('<%=rs("rec_id")%>')"  onChange="changedlistval('<%=rs("rec_id")%>')">
		</td>
		<TD width="5%" align="center" onClick="dolistact('<%=rs("rec_id")%>','is_live,display_order,display_time');">
		<img src="images/but_saved_yes.gif" name="img_<%=rs("rec_id")%>" border="0">
		</td>
		<TD width="5%" align="center" onClick="<% if rs("is_locked") & ""<>"1" then %>deleterec('<%=rs("rec_id")%>');<% end if %>">
		<% if rs("is_locked") & ""<>"1" then %>
        	<img src="images/trash.gif" border="0">
		<% else %>
        	<img src="images/padlock.png" border="0">		
		<% end if %>
		</td>


<%		numrecs=numrecs+1
		rs.Movenext
		If numrecs = numtoshow and NOT rs.EOF then
			flagnext=True
			exit do
		End If 
	Loop %>
	</table>
	<TABLE WIDTH="80%" align="center">
	<TR>
<%	IF pagenum>0 THEN %>
		<A HREF="slides.asp?<%=followpath(pagenum-1)%>">Previous Page</A>&nbsp;&nbsp;&nbsp;
<%	End If

	
	If totalrecords>numtoshow THEN %>
		&nbsp;&nbsp;
<%		pagerecs=INT(totalrecords/numtoshow)
		if totalrecords mod numtoshow>0 then pagerecs=pagerecs+1
		 %>
         Page: <input type="text" size="3" id="pagenumx" value="<%=pagenum+1%>" /> of <B><%=pagerecs%> 
         <input type="button" onClick="window.location.href='<%=slides%>?pagenum='+(document.getElementById('pagenumx').value-1)" value="Go" />
		
<%	IF flagnext THEN  %>
	&nbsp;&nbsp;	<A HREF="slides.asp?<%=followpath(pagenum+1)%>">Next Page</A>
<%	End If	

        End If
	rs.close
	Set rs=Nothing %>
	</TD></TR></TABLE>
</form>
<% End Sub








'*****************************************************************************************************************
'*****************************************************************************************************************

'VVVVVVV               VVVVVVV     VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV     VVVVVVV             VVVVVVV
' VVVVVVV             VVVVVVV      VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV     VVVVVVV             VVVVVVV
'  VVVVVVV           VVVVVVV              VVVVVVV            VVVVVVV                   VVVVVVV             VVVVVVV
'   VVVVVVV         VVVVVVV               VVVVVVV            VVVVVVVVVVVVVVVVV         VVVVVVV   VVVVVVV   VVVVVVV
'    VVVVVVV       VVVVVVV                VVVVVVV            VVVVVVVVVVVVVVVVV         VVVVVVV   VVVVVVV   VVVVVVV
'     VVVVVVV     VVVVVVV                 VVVVVVV            VVVVVVV                   VVVVVVV   VVVVVVV   VVVVVVV
'      VVVVVVV   VVVVVVV                  VVVVVVV            VVVVVVV                   VVVVVVV   VVVVVVV   VVVVVVV
'       VVVVVVV VVVVVVV            VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV      VVVVVVVVVVV   VVVVVVVVVVV
'        VVVVVVVVVVVVVV            VVVVVVVVVVVVVVVVVVVVV     VVVVVVVVVVVVVVVVVVVVV      VVVVVVVVVVV   VVVVVVVVVVV

'*****************************************************************************************************************
'*****************************************************************************************************************

'*************************************************
' View Record Area
'*************************************************

Sub ViewForm()
	Call GetVarsDB 
	Call LoadHeader
	Call PageHeader	%>
	<%=ConfirmLoseChanges%>

	<table bgcolor="<%=ViewTbThinLineBorder%>" cellspacing="1" cellpadding="0" width="100%" style="cursor:pointer;cursor:hand">
	<tr>
	<td  bgcolor="<%=ViewTbBg%>">
		<table cellpadding="0" cellspacing="0" width="100%">
		<tr >
		<td onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ViewTbBg%>'" width="50%" onClick="window.location.href=('slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>')" align="center" height="20"><A HREF="slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>" class=""tbl_header"">Back To Listing Page</A></td>
		<td width="1" bgcolor="<%=ViewTbThinLineBorder%>"><img src="images/spacer.gif" width="1"></td>
		<td width="50%" onMouseover="this.bgColor='<%=color_mouseover%>'" onMouseout="this.bgColor='<%=ViewTbBg%>'" bgcolor="<%=ViewTbBg%>" align="center" onClick="window.location.href=('slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>&editrecord=1')" height="20"><A HREF="slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>&editrecord=1">Edit This Record</A></td>
		</tr>
		</table>
	</td>
	</tr>
	</table><img src="images/spacer.gif" height="5"><br>
    
	<TABLE BORDER=0 WIDTH="100%" bgcolor="<%=ViewTbThinLineBorder%>" cellpadding="1" cellspacing="1">
    <tr><td bgcolor="<%=ViewTbBg%>">
	<table width="80%" align="center">	



	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("slide_title")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("slide_title"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("display_order")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("display_order"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("station_id")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("station_id"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("design_style")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("design_name"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("content_top")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("content_top"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("content_middle")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("content_middle"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("content_bottom")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("content_bottom"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("content_img_1")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("content_img_1"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("display_time")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("display_time"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("animation_style")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("anim_name"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("start_on")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("start_on"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("end_on")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("end_on"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("start_time")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("start_time"))%></td>
	</TR>
	<TR>
	<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc("end_time")%>:</B></td>
	<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN("end_time"))%></td>
	</TR>

	<%	IF newfields<>"" then Response.Write DrawNewFieldView %>	
	</table>
</td></tr></table>
<%	
End Sub











'********************************************************************************************************
'********************************************************************************************************

'EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEEEEEEE
'EEEEEEEEEEEEEEEEEEEEE     EEEEEEE        EEEEEEE    EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEEEEEEE
'EEEEEEE                   EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEEEEEEEEEEEE         EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEEEEEEEEEEEE         EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEE                   EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEE                   EEEEEEE        EEEEEEE           EEEEEEE                      EEEEEEE       
'EEEEEEEEEEEEEEEEEEEEE     EEEEEEE        EEEEEEE    EEEEEEEEEEEEEEEEEEEEE               EEEEEEE       
'EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEE               EEEEEEE       

'********************************************************************************************************
'********************************************************************************************************





'*************************************************
'Form Area
'*************************************************

function addtoheader %>
<script src="js/timepicker.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script language="JavaScript" src="cal2/calendar_us.js"></script> 
<link rel="stylesheet" href="cal2/calendar.css">
<link rel="stylesheet" type="text/css" href="kb/keyboard.css">
<script type="text/javascript" src="kb/keyboard.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<%
End function


Sub ShowForm(FormType)		
		'*************************************************
		' Chose a Record To Edit, Or Add New
		'************************************************* 
		formact=formtype
		Call LoadHeader
		Call PageHeader %>
		<%=ConfirmLoseChanges%>
		<%=SaveFuncJavascript%>		
        
        <script>
		function dupeSlide(recId) {
			if(confirm("Are you sure you want to duplicate this slide?")) {
				$.ajax({
					  url: "dupeslide.asp?rec_id="+recId,
					  context: document.body
					}).done(function() {
					  alert('Slide duplicated.');
					});
			}
			else {
				alert('Cancelled');	
			}
		};
		</script>
        
        
<%		Call DrawErrorthem
		
		 

		IF CheckForErrors<>"" THEN %>		
			<TABLE BORDER=0 align="center" ><TR><td valign="middle"><img src="images/alert.gif"></td><td><span style="color:#930000; font-size:14px;"><B>Please Correct The Following Errors: </font></b></td></tr><td></td><td><ul><%=CheckForErrors%></ul></td></tr></table>
<%		END IF 
		
		IF FormType="Add" THEN
			lstVN("display_time")=5
			lstVN("animation_style")=100001
		Else
			IF lstVN("start_time") & ""<>"" THEN  lstVN("start_time") = FormatDateTime(lstVN("start_time"),3)
			IF lstVN("end_time") & ""<>"" THEN  lstVN("end_time") = FormatDateTime(lstVN("end_time"),3)
		end if

		IF flagHasComboBox then Call DrawComboBox%>
		
	
		<form name="theform" action="slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>" method="POST" <% IF formtype<>"Search" then %>onSubmit="return ValidForm()"<% end if %>>
		<input type="hidden" name="changed" value="<%=IIF(checkforerrors="",0,1)%>">

        <TABLE BORDER=0 WIDTH="100%" bgcolor="<%=EditTbThinLineBorder%>" cellpadding="1" cellspacing="1">
		<tr><td bgcolor="<%=EditTbBg%>" align="center">       
		<% If FormType="Search" THEN %><B>To search for records, fill out all or part of any or all fields below and click "Search".</b><% end If %>

		<table width="90%" align="center">
		<TR><TD></TD><TD>
<%		IF FormType="Add" THEN %>
			<input type="hidden" name="addform" value="Add">
			<input type="Submit" onClick="return oksubmit();" name="savebutton" value="Save" >
			<input type="Submit" onClick="return oksubmit();" name="anotheradd" value="Add Another Record" >
<%		ElseIf FormType="Update" THEN %>			
			<input type="hidden" name="updateform" value="Update">
			<input type="Submit" onClick="return oksubmit();" name="savebutton" value="Save" >
			<% if lstVN("is_locked") & ""<>"1" then %>			
            	<input type="button" onClick="deleterec('<%=rec_id%>');" name="deleteform" value="Delete">
			<% end if %>
            
<%		ElseIf FormType="Search" THEN %>                        
			<input type="hidden" name="searchparams" value="Search">
			<input type="Submit" onClick="return oksubmit();" value="Search">
<%		End IF %>
		<input type="button" value="Back to Listing" onClick="window.location='slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>';">
        
<%		if formtype="Update" Then %>
			<input type="button" value="DUPLICATE SLIDE" style="margin-left:100px; background-color:#BDCCBB; font-weight:bold;" onclick="dupeSlide('<%=rec_id%>');" />        
<%		End if %>            
        
		</TD></TR>	



		<INPUT TYPE="HIDDEN" NAME="rec_id" value="<%=FixWords(rec_id)%>">
        <TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("is_live")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="checkbox" NAME="is_live" onClick="changesavebutton();addchange();" value="1" <%=IIF(lstVN("is_live") & ""="1","CHECKED","") %> <%=stylescript%>></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("slide_title")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="slide_title" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("slide_title"))%>" class="longtextfield" <%=stylescript%>></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("display_order")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="display_order" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("display_order"))%>" size=6 <%=stylescript%>></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("station_id")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="station_id" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("station_id"))%>" size=10 <%=stylescript%>></TD>		</tr>
        
        		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("days_active")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%">
        <% for x=0 to 6 %>
        <li style="float:left; width:200px;"><INPUT TYPE="checkbox" NAME="days_active" value="<%=x%>" <%=IIF(instr(lstVN("days_active"),x)>0,"CHECKED","")%>><%=weekdayname(x+1)%></li>
        <% next %>
        </TD>		</tr>
        
        
        
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("design_style")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><SELECT NAME="design_style" onChange="changesavebutton();addchange();"  <%=stylescript%>>
										 <option value="">None</option>
		<%		Set rsLTS=Server.CreateObject("ADODB.RecordSet")
				sql="SELECT * FROM design_styles ORDER BY  rec_id"
				rsLTS.Open sql,conn
				Do Until rsLTS.EOF 
					seloption=IIF(Instr(lstVN("design_style"),rsLTS("rec_id"))>0,"Selected","") %>
					<OPTION VALUE="<%=rsLTS("rec_id")%>" <%=seloption%>><%=rsLTS("design_name")%></OPTION>
		<%			rsLTS.MoveNext
				Loop
				rsLTS.Close
				Set rsLTS=Nothing %>				</select></TD>		</tr>

		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("content_img_1")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="content_img_1" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("content_img_1"))%>" size=25 <%=stylescript%>><br><input type="submit" name="uploadimg" value="Upload File" onClick="return oksubmit();">
		<INPUT TYPE="HIDDEN" NAME="content_img_2" value="<%=FixWords(content_img_2)%>"><br />
		<strong>Please Note:</strong> The image uploaded here will appear as the background for the content in the "Middle Box" below, regardless of the "Design Style" chosen above. Image resolutions should be:<br />
		<br />
		Standard (3 Box) - <strong>1000 x 500</strong><br />
		One Box	 - <strong>1000 x 1000</strong><br />
        Two Box - <strong>1000 x 800</strong>
        <% IF session("upload") = 1  THEN
				session("upload") = 0 %>
                <br /><br />
		        <div id="upload_div">
		        <iframe width="450" height="250" frameborder="0" src="getfile.asp?thesku=<%=rec_id%>&dbname=<%=db_name%>&db_field=<%=uploadimage%>">
		        </iframe>
		        </div>       
<%		End If %>
		</TD>		</tr>                
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("display_time")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="display_time" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("display_time"))%>" size=6 <%=stylescript%>></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("animation_style")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><SELECT NAME="animation_style" onChange="changesavebutton();addchange();"  <%=stylescript%>>
										 <option value="">None</option>
		<%		Set rsLTS=Server.CreateObject("ADODB.RecordSet")
				sql="SELECT * FROM animation ORDER BY  anim_name"
				rsLTS.Open sql,conn
				Do Until rsLTS.EOF 
					seloption=IIF(Instr(lstVN("animation_style"),rsLTS("anim_id"))>0,"Selected","") %>
					<OPTION VALUE="<%=rsLTS("anim_id")%>" <%=seloption%>><%=rsLTS("anim_name")%></OPTION>
		<%			rsLTS.MoveNext
				Loop
				rsLTS.Close
				Set rsLTS=Nothing %>				</select></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("start_on")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="start_on" value="<%=FixWords(lstVN("start_on"))%>" size="10" onKeyup="changesavebutton();" onChange="javascript:addchange();" onKeydown="return dFilter (event.keyCode, this, '##/##/####');"  <%=stylescript%>>
		<script language="javascript"> new tcal ({ 'formname': 'theform','controlname': 'start_on' })</script></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("end_on")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="end_on" value="<%=FixWords(lstVN("end_on"))%>" size="10" onKeyup="changesavebutton();" onChange="javascript:addchange();" onKeydown="return dFilter (event.keyCode, this, '##/##/####');"  <%=stylescript%>>
        <script language="javascript"> new tcal ({ 'formname': 'theform','controlname': 'end_on' })</script></TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("start_time")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><INPUT TYPE="TEXT" NAME="start_time" id="start_time" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("start_time"))%>" size=8 <%=stylescript%>>
        <IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,start_time)" STYLE="cursor:hand">
        </TD>		</tr>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("end_time")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="75%"><INPUT TYPE="TEXT" NAME="end_time" id="end_time" onKeyup="changesavebutton();"  onChange="addchange();" value="<%=FixWords(lstVN("end_time"))%>" size=8 <%=stylescript%>>
         <IMG SRC="images/timepicker.gif" BORDER="0" ALT="Pick a Time!" ONCLICK="selectTime(this,end_time)" STYLE="cursor:hand">
        </TD>		</tr>
        <% if lstVN("is_locked") & ""="1" and left(request.ServerVariables("REMOTE_ADDR"),"8") & ""<>"10.10.3." then 
			hidecontent = "style='display:none;'" %>
            <TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B>Content Locked</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%">The content elements of this slide are system controlled.</td>
        <% end if  %>
         <tr <%= hidecontent %>><td bgcolor="<%=EditTbLeftCol%>" align="right"><strong>CONTENT:</strong></td>
        <td bgcolor="<%=EditTbRightCol%>" >
        <div id="tab_1" style="background-color:#000; color:#FFF; width:100px;  text-align:center; padding:10px; float:left; margin-right:10px;" onclick="ShowBox(1);">
        Top Box
        </div>
        <div id="tab_2" style="background-color:#999; color:#FFF; width:100px;  text-align:center; padding:10px; float:left; margin-right:10px;" onclick="ShowBox(2);">
        Middle Box
        </div>
         <div id="tab_3" style="background-color:#999; color:#FFF; width:100px;  text-align:center; padding:10px; float:left; margin-right:10px;" onclick="ShowBox(3);">
        Bottom Box
        </div>
        </td></tr> 
        <TR id="box_1" <%= hidecontent %> >
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("content_top")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><textarea NAME="content_top" id="content_top"   class="keyboardInput" cols=35 rows=5 onKeyup="changesavebutton();" onChange="addchange();"  <%=stylescript%>><%=FixWords(lstVN("content_top"))%></textarea></TD>		</tr>
		<TR id="box_2" style="display:none;" <%= hidecontent %>>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("content_middle")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><textarea NAME="content_middle" id="content_middle" cols=40 rows=15 onKeyup="changesavebutton();" onChange="addchange();"  <%=stylescript%>><%=FixWords(lstVN("content_middle"))%></textarea></TD>		</tr>
		<TR id="box_3" style="display:none;" <%= hidecontent %>>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="15%" height="25"><B><%=FieldDesc("content_bottom")%>:</B></td>
		<td bgcolor="<%=EditTbRightCol%>" align="left" width="85%"><textarea NAME="content_bottom" id="content_bottom" cols=35 rows=5 onKeyup="changesavebutton();" onChange="addchange();"  <%=stylescript%>><%=FixWords(lstVN("content_bottom"))%></textarea></TD>		</tr>
		<TR>

<%		IF newfields<>"" then DrawNewFieldEdit %>
		<TR><TD></TD><TD>
<%		IF FormType="Add" THEN %>
			<input type="hidden" name="addform" value="Add">
			<input type="Submit" onClick="return oksubmit();" name="savebutton2" value="Save" >
			<input type="Submit" onClick="return oksubmit();" name="anotheradd2" value="Add Another Record" >
<%		ElseIf FormType="Update" THEN %>			
			<input type="hidden" name="updateform" value="Update">
			<input type="Submit" onClick="return oksubmit();" name="savebutton2" value="Save" >
			<% if lstVN("is_locked") & ""<>"1" then %>			
            	<input type="button" onClick="deleterec('<%=rec_id%>');" name="deleteform" value="Delete">
			<% end if %>
<%		ElseIf FormType="Search" THEN %>                        
			<input type="hidden" name="searchparams" value="Search">
			<input type="Submit" onClick="return oksubmit();" value="Search">
<%		End IF %>
		<input type="button" value="Back to Listing" onClick="window.location='slides.asp?<%=followpath(pagenum)%>&rec_id=<%=rec_id%>';">
		</td></tr></table></td></tr></table>	
		</form>
		<script language="javascript">
		function ShowBox(wd) {
			var bcol = "#999"
			var bdisplay = "none"
			for(i=1;i<4;i++) {
				i==wd ? bcol="#000" : bcol="#999"
				i==wd ? bdisplay="" : bdisplay="none"
				document.getElementById("tab_"+i).style.backgroundColor = bcol 
				document.getElementById("box_"+i).style.display = bdisplay
			}
		} 
		

		CKEDITOR.replace( 'content_middle', {
		height: "700", width: "99%",
		toolbar : 
		 [
        ['Source','NewPage','Preview'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Scayt'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Table','HorizontalRule','SpecialChar','PageBreak'],
        
		'/',
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],['Font','FontSize','Bold','Italic','Underline', 'Strike'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['Maximize']		
	    ]
		
		});
		
		CKEDITOR.replace( 'content_top', {
		height: "200", width: "99%",
		toolbar : 
		 [
        ['Source','NewPage','Preview'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Scayt'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Table','HorizontalRule','SpecialChar','PageBreak'],
        
		'/',
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],['Font','FontSize','Bold','Italic','Underline','Strike'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['Maximize']		
	    ]
		
		});		
		
		
		CKEDITOR.replace( 'content_bottom', {
		height: "200", width: "99%",
		toolbar : 
		 [
        ['Source','NewPage','Preview'],
        ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Scayt'],
        ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        ['Table','HorizontalRule','SpecialChar','PageBreak'],
        
		'/',
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],['Font','FontSize','Bold','Italic','Underline','Strike'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['Maximize']		
	    ]
		
		});				

		
		</script>
<%
End Sub
%>





















<%



'**************************************************************************************************
'**************************************************************************************************
'*************************************************
' End of HTML Writing to the page
'*************************************************
'**************************************************************************************************
'**************************************************************************************************






















	
'**************************************************************************************************
'**************************************************************************************************

'OOOOOOOOOOO  OOOO   OOOO  OOOO   OOOO   OOOOOOOOO   OOOOOOOOOOOO   OOO    OOOOOOOOO   OOOO   OOOO   OOOOOOOOO
'OOOO         OOOO   OOOO  OOOOOO OOOO  OOOOO  OOOO  OOOOOOOOOOOO  OOOOO  OOOOOOOOOOO  OOOOOO OOOO  OOOOO    OO
'OOOOOOOO     OOOO   OOOO  OOOOOOOOOOO  OOOO             OOOO      OOOOO  OOO     OOO  OOOOOOOOOOO  OOOOOO
'OOOOOOOO     OOOO   OOOO  OOOOOOOOOOO  OOOO             OOOO      OOOOO  OOO     OOO  OOOOOOOOOOO     OOOOOOOO
'OOOO         OOOOOOOOOOO  OOOO  OOOOO  OOOOO  OOOO      OOOO      OOOOO  OOOOOOOOOOO  OOOO  OOOOO  OO    OOOOO
'OOOO          OOOOOOOOO   OOOO   OOOO   OOOOOOOOO       OOOO       OOO    OOOOOOOOO   OOOO   OOOO   OOOOOOOOO


'8888   8888   888888888   8888888888    8888888888
'8888   8888  8888   8888  8888   8888  88888     88
'8888   8888  8888   8888  8888   8888   88888888
'88888 88888  88888888888  8888888888      88888888
' 888888888   8888   8888  8888 888888  88     88888
'   88888     8888   8888  8888   8888   8888888888

'**************************************************************************************************
'**************************************************************************************************
' Subs to Get variables from form, db, 
' full recordset list	sql=Replace(sql,"SELECT * FROM","SELECT COUNT(*) AS numrecs FROM")

Sub GetVarsDb()
	Set rs=Server.CreateObject("ADODB.Recordset")


	sql="SELECT slides.*, design_styles.design_name , animation.anim_name " &_
		" FROM slides " &_ 
		" LEFT OUTER JOIN design_styles ON slides.design_style=design_styles.rec_id" &_ 
		" LEFT OUTER JOIN animation ON slides.animation_style=animation.anim_id" &_ 
		" WHERE (slides.rec_id=" & rec_id & ")"


	rs.Open sql,conn
	If Not rs.EOF Then
		for each fname in rs.Fields
			lstVN.Add FormReady(fname.name), FormReady(fname)
		Next
	Else
		FlagNoRecord=True
	End If
	
	rs.Close
	Set rs=Nothing
End Sub

Function GetSqlShowAll()

	IF sortorder<>"" then 
		sortorder1=Replace(sortorder,"-"," ")
	else
		sortorder1="slides.display_order"
	End If

	sql="SELECT slides.*, design_styles.design_name , animation.anim_name " &_
		" FROM slides " &_ 
		" LEFT OUTER JOIN design_styles ON slides.design_style=design_styles.rec_id" &_ 
		" LEFT OUTER JOIN animation ON slides.animation_style=animation.anim_id" &_ 
		" WHERE (slides.rec_id IS NOT NULL) :1 ORDER BY " & sortorder1

	IF filtername<>"" THEN
        if filterby="" then filterby="slides.display_order"
		sql=Replace(sql,":1"," AND " & filterby & " LIKE '" & replace(filtername,"'","''") & "%' :1")
   	End IF
	sql=replace(sql,":1","")


	IF advsearch<>"" then sql=advsearch
	sql=Replace(sql,":1","")
	GetSqlShowAll=sql
End Function


Function ErrorthemManual()
	'Put your manual Error checking in here...
	
	'IF lstV("field")<>"" THEN 
	'	If len(lstV("field"))<1  and not allownulls THEN 
	'		ErrorthemManual=ErrorthemManual & "<LI>" & FieldDesc("field") & " can't be less than 3 characters long."
	'	ElseIf len(lstV("field"))>50 THEN 
	'		ErrorthemManual=ErrorthemManual & "<LI>" & FieldDesc("field") & " can't be more than 50 characters long."
	'	END IF
	'End If 

End Function

%>
