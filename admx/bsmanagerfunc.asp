<%
'operators
Dim  formact, totalrecords, filtername, filterby, sortorder, pagenum, numtoshow, checkforerrors, flagnorecord, deletefromlist, nojavaform, allownulls

'Triggers
addrec=Request.QueryString("addrec")
addform=Request.Form("addform")
anotheradd=Request.Form("anotheradd")

viewrecord=Request.QueryString("viewrecord")
editrecord=Request.QueryString("editrecord")
updateform=Request.Form("updateform")

deleteform=Request.Form("deleteform")
deletefromlist=Request.QueryString("deletefromlist")
confirmdelete=Request.QueryString("confirmdelete")

shownothing=Request.QueryString("shownothing") 
searchf=Request.QueryString("searchf")
searchparams=Request.Form("searchparams")



'------------------------------------
' Standard Filters
'------------------------------------
filtername=Request.QueryString("filtername")
filterby=Request.QueryString("filterby")
sortorder=Request.QueryString("sortorder")
populateparent=Request.QueryString("populateparent")
IF db_name="" then db_name="global"

IF filterby<>"" then
	Response.Cookies(db_name)("filterby")=filterby
	Response.Cookies(db_name).Expires=#12-12-2020#
End If
filterby=Request.Cookies(db_name)("filterby")

IF filtername<>"" or Instr(REquest.Servervariables("QUERY_STRING"),"filtername")>0 then
	Response.Cookies(db_name)("filtername")=filtername
	Response.Cookies(db_name).Expires=#12-12-2020#
	Response.Cookies(db_name)("advsearch")=""
End If
filtername=Request.Cookies(db_name)("filtername")

IF sortorder<>"" then
	Response.Cookies(db_name)("sortorder")=sortorder
	Response.Cookies(db_name).Expires=#12-12-2020#
End If
sortorder=Request.Cookies(db_name)("sortorder")
advsearch=Request.Cookies(db_name)("advsearch")

IF populateparent<>"" then
	Response.Cookies("populateparent")=populateparent
End If
populateparent=Request.Cookies("populateparent")



Function ConfirmLoseChanges %>
	<SCRIPT LANGUAGE=JavaScript FOR=window EVENT=onbeforeunload>
	ischanged=document.theform.changed.value;
	if (ischanged!="0") {
	var strMsg = 'Since you have not saved your changes, leaving this page with will cause you to lose any information you have entered.'  
	window.event.returnValue=strMsg;  }
	</SCRIPT>
<%
End Function


Function PopulateParentVal(thedb, theorder, formfield, fieldid, fieldval)
	Set rs2=Server.CreateObject("ADODB.RecordSet")
	sql="SELECT count(*) as numrecs FROM " & thedb
	rs2.Open sql, conn
	numrecs1=rs2("numrecs")
	rs2.close
	sql="SELECT * FROM " & thedb & " ORDER BY " & fieldval
	rs2.Open sql, conn%>
	<SCRIPT>
	while (opener.document.theform.<%=formfield%>.options.length) opener.document.theform.<%=formfield%>.options[0]=null;
	opts=opener.document.theform.<%=formfield%>
	opts.length = <%=numrecs1+1%>;
    	opts[0].text = "None";
	opts[0].value = "0";
<%		rs2.MoveFirst:x=1
	Do Until rs2.EOF %>
    	opts[<%=x%>].text = "<%=rs2(fieldval)%>";
	opts[<%=x%>].value = "<%=rs2(fieldid)%>";
<%			rs2.MoveNExt:x=x+1
	Loop %>
	</SCRIPT>
<%		rs2.Close
	Set rs2=Nothing
End Function


Function PopulateParentJavascript %>
	<script>
		function confirmnav(thelink,linkname) {
<%		IF editrecord<>"" then %>
			if (confirm("By choosing to Edit "+linkname+", whichever selection is highlighted within "+linkname+" will be lost. If you still want to edit, click OK otherwise, click Cancel.")) {
				window.open(thelink) 
			}
<%		else %>
			window.open(thelink)		
<%		End If %>			
		}
	</script>
<%
End Function



Function ListUpdateJavascript %>
	<script>
	function dolistact(therec,thefields) {
		theimg1=eval("document.img_"+therec)
		imgname=theimg1.src
		imgname=imgname.substring(imgname.length-7)
		if(imgname!="yes.gif") {
			document.theform.changed.value="0"
			newfields=thefields.split(",")
			allfields=""
			allfieldvals=""
			for(x=0;x<newfields.length;x++) {
				thefieldname="f_"+therec+"_"+newfields[x];
				thefval=eval("document.theform."+thefieldname);
				if(thefval.type=='checkbox') {
					finalval=0
					if(thefval.checked==true) finalval=1 
				}
				else {
					finalval=thefval.value.replace(/,/g,"|�|")
					finalval=finalval.replace(/&/g,"|$|")
					finalval=finalval.replace(/%/g,"|@@|")
				}
				allfieldvals=allfieldvals+finalval+","
				}
			listupdater.location.href="<%=pagename%>?shownothing=1&ident="+therec+"&allfields="+thefields+"&allfieldvals="+allfieldvals 
		}
	}
	
	function changedlistval(theimg) {
		ischanged=document.theform.changedf.value
		document.theform.changedf.value=ischanged+theimg+","
		document.theform.changed.value="1"
	}
	
	function changesaveimg(theimg) {
		theimg1=eval("document.img_"+theimg)
		theimg1.src="images/but_saved_no.gif"
		}
	
	</script>

<%
End Function

Function SaveFuncJavascript %>
	<script>
	function addchange() {
<%	IF formact<>"Search" Then %>
		document.theform.changed.value=1;	
<%	End If %>	
		}
		
	function changesavebutton() {
<%	IF formact<>"Search" Then %>
		document.theform.savebutton.disabled=0;
		document.theform.savebutton2.disabled=0;
<%	End If %>	
<%	IF addrec="1" then %>	
		document.theform.anotheradd.disabled=0;
		document.theform.anotheradd2.disabled=0;
<%	End If %>	
	}
	
	function oksubmit() {
		document.theform.changed.value=0;
		return(true);
		 }	
	</script>


<%
End Function

Function FixForToolTip(thestring)
	thestring=thestring & ""
	newstring=replace(thestring,"'","�")
	newstring=replace(newstring,"""","��")
	newstring=replace(newstring,VbCrLf,". ")
	FixForTooltip=newstring
End Function

Function FormReady(thevar)
	If isnull(thevar) then
		FormReady=""
	Else
		FormReady=Cstr(thevar & "")
	End If
End Function

Function FieldDesc(thestring)
	newstring=lstFN(thestring)
	IF newstring<>"" then FieldDesc=mid(newstring,1,instr(newstring,",")-1)
End Function


Function MakeFieldList
	For Each fieldname in lstFN
		fprops=split(lstFN(fieldname),",")
		fixvarstype=fprops(ubound(fprops))
		IF fixvarstype<>"0" then 
			fixvarstype="~" & fixvarstype
		else
			fixvarstype=""
		end if
		MakeFieldList=MakeFieldList & fieldname & fixvarstype & ","
	Next
	MakeFieldList=mid(MakeFieldList,1,len(MakeFieldList)-1)
End Function


Function ListUpdate
	nojavaform=true
	allfields=Request.QueryString("allfields")
	if allfields="" then exit function
	listfields=split(allfields,",")
	allfieldvals=Request.QueryString("allfieldvals")
	allfieldvals=mid(allfieldvals,1,len(allfieldvals)-1)
	listvals=split(allfieldvals,",")

	for x=0 to ubound(listfields)
		lstVN.Add FormReady(listfields(x)), FormReady(listvals(x))
	Next
	
	AllowNulls=True
	CheckForErrors=Errorthem
	IF checkforerrors="" then
		Set rs=Server.CreateObject("ADODB.RecordSet")
		sql="UPDATE " & db_name & " SET " 
	
		for x=0 to ubound(listfields)
			thefixvars=right(lstFN(listfields(x)),1)
			incomma=IIF(x=ubound(listfields),"",", ")
			sql=sql & listfields(x) & "=" & FixVars(lstVN(listfields(x)),thefixvars) & incomma
		next
		sql=sql & " WHERE " & ident & "=" & Request.QueryString("ident")
		sql=replace(sql,"|�|",",")		
		sql=replace(sql,"|$|","&")	
		sql=replace(sql,"|@@|","%")	
		rs.Open sql, conn
		Set rs=Nothing %>
		<script>
		theimg=eval("parent.document.img_<%=Request.QueryString("ident")%>")
		theimg.src="images/but_saved_yes.gif"
		ischanged=parent.document.theform.changedf.value
		ischanged=ischanged.replace("<%=Request.QueryString("ident") & ","%>","")
		parent.document.theform.changedf.value=ischanged
		if(ischanged=="") {
			parent.document.theform.changed.value="0" }
		else {
			parent.document.theform.changed.value="1" }
		</script>
<%	else %>
		<script>
		parent.alert("<%=replace(checkforerrors,"<LI>","\n")%>")
		</script>	
<%	end if

End Function


Function GetTotalRecords(sql)
	locfrom=instr(sql,"FROM")
	cropsql=mid(sql,locfrom)
	locorderby=instr(cropsql,"ORDER BY")
	IF locorderby>0 then cropsql=mid(cropsql,1,locorderby-1)
	countsql="SELECT COUNT(*) AS numrecs " & cropsql

	Set rsGetCount=conn.Execute(countsql)
	IF NOT rsGetCount.EOF then 
		gettotalrecords=rsGetCount("numrecs")
	else
		gettotalrecords=0
	end if
	rsGetCount.Close
	Set rsGetCount=Nothing
END Function

Sub GetFormVars()
	for each fname in Request.Form
		lstVN.Add FormReady(fname), FormReady(Request.Form(fname))
	Next
End Sub

Sub GetQryVars()
	for each fname in Request.QueryString
		lstVN.Add FormReady(fname), FormReady(Request.QueryString(fname))
	Next
End Sub

Function GenerateSearchSQL
	basesql=GetSqlShowAll
	endsql=mid(basesql,instr(basesql,"ORDER BY"))
	basesql=mid(basesql,1,instr(basesql,"WHERE")-1)
	bfields=Split(field_names,",")
	sql=""
	For Each fname in lstFN
		fvars=right(lstFN(fname),1)
		thevar=Request.Form(fname)
		if fvars=0 then
			IF len(thevar)>0 then sql=sql & "AND (" & fname & " LIKE '" & replace(thevar,"'","''") & "%') "
		else
			IF len(thevar)>0 then sql=sql & "AND (convert(varchar," & db_name & "." & fname & ") LIKE '" & replace(thevar,"'","''") & "%') "
		end if
	Next
	sql=basesql & " WHERE (" & db_name & "." & ident & " IS NOT NULL) :1 " &  sql & endsql
	Response.Cookies(db_name)("advsearch")=sql
End Function



Function Errorthem()
	For Each fieldname in lstFN
		fprops=split(lstFN(fieldname),",")
		field_desc=fprops(0):field_type=fprops(1):field_len=fprops(2):field_req=fprops(3):field_value=lstVN(fieldname):in_list=fprops(4)
		IF field_req="required" and (in_list="list" and allownulls=False) THEN
			IF isnull(field_value) or field_value="" then ErrorThem=Errorthem & "<LI>'" & field_desc & "' can't be left blank."
		End If 
		
		If field_value<>"" then
			IF field_type="varchar" or field_type="text" then
				IF len(field_value)>int(field_len) then errorthem=errorthem & "<LI>'" & field_desc & "' can't be more than " & field_len & " characters long."
				if len(field_value)>1 then
				end if
			elseif field_type="datetime" then
				If (ubound(split(field_value,"/"))<1 OR Not Isdate(field_value)) and instr(field_value,":")<1 THEN 
					errorthem=errorthem & "<LI>'" & field_desc & "' must be a valid date in the format of mm/dd/yyyy"
				END IF
			elseif field_type="int" or field_type="float" or field_type="money" then
				If Not isnumerals(field_value) THEN 
					errorthem="<li>'" & field_desc & "' must be a valid number (not text or other characters)."
				END IF
			End If
		END IF
		
	Next
	errorthem=errorthem & ErrorthemManual
End Function 




Sub DrawComboBox()%>
	<iframe src="floatcombo.asp" name="floater" width="0" height="0" ></iframe>
	<script language="javascript">
	function poster(theformfield,thetable) {
	   document.theform.changed.value=1;
	   tval=eval("document.theform."+theformfield+".value")
		if(tval.length==1) {
	    	tval1=tval.substring(0,1)
		    floater.location.href='floatcombo.asp?let='+tval1+'&theformf='+theformfield+'&thetb='+thetable; }
	}
	</script>
<%	
End Sub 




Sub DrawErrorthem %>		
<script type="text/javascript" language="JavaScript">
<!--
function ValidForm() {
<%	For Each fieldname in lstFN
		fprops=split(lstFN(fieldname),",")
		field_desc=fprops(0):field_type=fprops(1):field_len=fprops(2):field_req=fprops(3):field_value=lstVN(fieldname)
		IF field_req="required" and not allownulls THEN %>
			if (theform.<%=fieldname%>.value == "") {
				alert("Please enter a value for '<%=field_desc%>'.")
				theform.<%=fieldname%>.focus()
				return(false) } 
<%		ELSeIf field_red="optional" then
			'nutin right now
		END IF
		
	Next %>

	}

-->
</script>

<%

End Sub

%>