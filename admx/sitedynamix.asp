<%

'colors
PageBgcolor="#FFFFFF"
color_mouseover="#FFF0E0"

MainTBThinLineBorder="#CCCCCC"
MainTbBg="#FFFFFF"

FilterTBThinLineBorder="#CCCCCC"
FilterTbBg="#FFFFFF"

ListTBThinLineBorder="#CCCCCC"
ListTbBg="#FFFFFF"

ViewTbThinLineBorder="#CCCCCC"
ViewTbBg="#FFFFFF"
ViewTbLeftCol="#DDDDDD"
ViewTbRightCol="#EEEEEE"

EditTbThinLineBorder="#CCCCCC"
EditTbBg="#FFFFFF"
EditTbLeftCol="#DDDDDD"
EditTbRightCol="#EEEEEE"


StyleScript=" onFocus=""javascript:this.style.backgroundColor='#ffffcc';"" onBlur=""javascript:this.style.backgroundColor='#ffffff'"" "


Sub LoadHeader
	'*************************************************
	' Header Include, and any other header issues in
	' this sub, but it must be called explicitly
	'************************************************* %>
	<!--#include file="htmlheader.asp"-->
	<% IF FlagInputMask Then %><script type='text/javascript' src='dfilter.js'></script><% End If %>			
	<% IF FlagAddingMachine Then %><script type='text/javascript' src='addmachine.js'></script><% End If %>
	<% IF FlagHasPopupCalendar Then %><script language='javascript' src='popupcalendar/popcalendar.js'></script><% End If %>
	<SCRIPT LANGUAGE="Javascript" TYPE="TEXT/JAVASCRIPT">
	function deleterec(therec) {
		if (confirm("Are you sure that you want to delete this record?")) {
			document.theform.changed.value=0;
			window.location="<%=pagename%>?<%=followpath(pagenum)%>&confirmdelete=1&deletefromlist=<%=deletefromlist%>&<%=ident%>="+therec; }
		}
	</script>
	<table cellpadding="5" cellspacing="1" bgcolor="<%=MainTBThinLineBorder%>" align="center" width="98%">
	<tr>
	<Td bgcolor="<%=color_mouseover%>" height="20" class="page_header">
	&nbsp;&nbsp;<%=manager_name%>
	</td></tr>
	<tr>
	<Td bgcolor="<%=MainTbBg%>">
<%	
End Sub

Sub LoadHeaderClose %>
	</td></tr></table>
	<!--#include file="htmlheaderclose.asp"-->

<% End Sub

'*************************************************
' Top of the page for non-list view pages
'*************************************************
Sub PageTitles %>


<% End Sub




Function DrawNewFieldView
	newf=split(newfields,",")
	For x=0 to ubound(newf) %>
		<TR>
		<TD align="right" bgcolor="<%=ViewTbLeftCol%>" width="25%" height="25"><B><%=FieldDesc(newf(x))%>:</B></td>
		<td  bgcolor="<%=ViewTbRightCol%>" align="left" width="75%"><%=FixWords(lstVN(newf(x)))%></td>
		</TR>	
<%	Next 
End Function




Function DrawNewFieldEdit
	newf=split(newfields,",")
	For x=0 to ubound(newf) 
		fprops=split(lstFN(newf(x)),",")
		field_desc=fprops(0):field_type=fprops(1):field_len=fprops(2):field_req=fprops(3):field_value=lstVN(fieldname):in_list=fprops(4)%>
		<TR>
		<TD align="right" bgcolor="<%=EditTbLeftCol%>" width="25%" height="25"><B><%=field_desc%>:</B></td>
		<td  bgcolor="<%=EditTbRightCol%>" align="left" width="75%">
<%		IF field_type="datetime" or field_type="money" or field_type="int"  then 
			sizer=10:typer="text"
		elseIf field_typer="text" then
			cols=55:rows=10:typer="textarea"
		elseIf field_len<50 then 
			sizer=field_len:typer="text"			
		elseIf field_len<150 then 
			sizer=55:typer="text"
		elseIf field_len>150 then
			cols=35:rows=5:typer="textarea"
		End If 
		
		IF typer="textarea" then %>
			<textarea NAME="<%=newf(x)%>" cols=<%=cols%> rows=<%=rows%>  onKeydown="changesavebutton();" onChange="addchange();"><%=FixWords(lstVN(newf(x)))%></textarea>				
<%		Else %>
			<INPUT TYPE="TEXT" NAME="<%=newf(x)%>"  onKeydown="changesavebutton();" onChange="addchange();" value="<%=FixWords(lstVN(newf(x)))%>" size="<%=sizer%>">
<%		End If %>		
		
		</td>
		</TR>	
<%	Next 
End Function



%>