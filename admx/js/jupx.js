
	var last_request_status = "";
	var gopherLoc = "http://dev.blueswitch.com/kid_city/jupdater_gopher.asp";
	var last_updated_id = "";

	
	jupxaddEvent(window,"load",jupxTagElements);
	
	function jupxTagElements() { 
		whichelems = document.getElementsByTagName("input")		// pull all elements that are input fields
		for(i=0;i<whichelems.length;i++) {						// iterate through em
			elemobj = whichelems[i];							// 
			if(elemobj.name) {									// has an id.
				if(elemobj.name.substr(0,5) == "jupx|") {			// is this a jupx obj
					ttype = elemobj.type;
					switch(ttype) {
						case "text":				
							jupxaddEvent(elemobj,"change",jupxfireEvent);
							break;
						case "checkbox":
							jupxaddEvent(elemobj,"click",jupxfireEvent);
							break;
						case "radio":
							jupxaddEvent(elemobj,"click",jupxfireEvent);
							break;							
					}
				}
			}
		}
	}
	
	function jupxfireEvent() {
		ttype = this.type;
		//alert(ttype);
		switch(ttype) {
			case "text":
				jupxUpdate(this.name, this.value);
				break;
			case "checkbox":
				
				if(this.checked) {
					jupxUpdate(this.name, this.value);
				}
				else {
					jupxUpdate(this.name, "");
				}
				break;		
			case "radio":
				
				if(this.checked) {
					jupxUpdate(this.name, this.value);
				}
				else {
					jupxUpdate(this.name, "");
				}
				break;					
		}
	}
	
	
	function jupxaddEvent(obj,type,fn) {
		/*
		if (obj.addEventListener) {
				obj.addEventListener(type,fn,false);
				return true;
		} 
		else if (obj.attachEvent) {
			obj['e'+type+fn] = fn;
			obj[type+fn] = function() { obj['e'+type+fn]( window.event );}
			var r = obj.attachEvent('on'+type, obj[type+fn]);
			return r;
		} 
		else { 
		*/
			obj['on'+type] = fn;
			return true;
		//}
	}	

	function jupxUpdate(tid, tval) {
		last_request_status = "Attempting to update info..."
		if(document.getElementById("jupx_last_request_status")) { 
			document.getElementById("jupx_last_request_status").innerHTML = last_request_status;
		}
		bx = tid.split("|");
		if(bx.length<4) {
			return false;
		}
		tabn = bx[1];
		fieldn = bx[2];
		ident = bx[3];
		recid = bx[4];
		last_updated_id =  recid;
		last_request_status = "Passing data to database..."
		if(document.getElementById("jupx_last_request_status")) { 
			document.getElementById("jupx_last_request_status").innerHTML = last_request_status;
		}		
		jupxgetURL(gopherLoc+"?tname="+tabn+"&fname="+fieldn+"&ident="+ident+"&fid="+recid+"&fval="+tval,"");
	}
	

   var jupxhttp_request = false;
   function jupxmakeRequest(url, parameters) {
      http_request = false;
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         return false;
      }
      http_request.onreadystatechange = jupxalertContents;
      http_request.open('GET', url + parameters, true);
      http_request.send(null);
   }

   function jupxalertContents() {
      if (http_request.readyState == 4) {
         if (http_request.status == 200) {
			last_request_status = http_request.responseText;
			//alert(last_request_status)
			document.getElementById("tot_"+last_updated_id).innerHTML = last_request_status;
			updateTotals(last_updated_id);
			if(document.getElementById("jupx_last_request_status")) { 
					document.getElementById("jupx_last_request_status").innerHTML = "Data saved successfully.";
				}
         } else {
              last_request_status = "There was a problem with the update. Data was incorrect.";
			  document.getElementById(last_field).value = last_val;
			  alert(last_request_status);
			if(document.getElementById("jupx_last_request_status")) { 
					document.getElementById("jupx_last_request_status").innerHTML = "Error - last change was not saved.";
				}			  
         }
      }
   }
   
   
   function jupxgetURL(turl, tvars) {
      jupxmakeRequest(turl, tvars);
   }
